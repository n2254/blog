---
title: "Creando mi página con Hugo"
date: 2023-02-03
---

Versión 0.1, del 3 de febrero de 2023

# Comandos para producir la página web
## Previsualización de la página web

Con el comando
hugo server
se puede visualizar en tiempo real los cambios hechos a la página web, sin necesidad de reconstruirla. La página web se puede acceder en
localhost:1313

## Crear la página web y subirla a internet

El comando
hugo
crea los archivos definitivos de la página web, que se almacenan en la carpeta
public.

Una vez creados dichos archivos, se puede hacer un commit para registrar los cambios en git y enviarlos al repositorio remoto con
git push.

# Los directorios

## public
La carpeta
public
es la que almacenará todos los archivos HTML, JS, CSS y demás que se generen con la página web: lo que realmente se sirve desde un servidor y se visualiza en el navegador. Los contenidos de la carpeta se actualizan cada vez que usamos el comando
hugo
y construimos la página web a partir de las distintas plantillas, archivos de contenido y de configuración.

## content
La carpeta
content
almacena los archivos de contenido de nuestra página web, es decir, el texto de cada una de las páginas accesibles desde el navegador, lo que nosotros interpretaríamos como _nuestra_ página. Éstos son archivos Markdown.

Las distintas subcarpetas permiten crear secciones en nuestra página web, a las que pertenecerán las páginas correspondientes a los archivos de contenido que haya en dichas carpetas. También pueden usarse para definir categorías.

### _index.md
Los archivos
_index.md
permiten añadir texto propio a las páginas de sección, categorías, o listas en general. En principio, el propósito de estas páginas es servir de listado de otras páginas más importantes (por ejemplo, una sección que muestre ciertas páginas de un blog), pero si queremos que muestre algún texto o imágenes introductorios, este texto irá en un archivo _index.md situado en el interior del directorio correspondiente.

## layouts
En la carpeta
layouts
se guardan las plantillas para crear las páginas de nuestro sitio web. Aquí se indican los elementos comunes para varias páginas que compartan un determinado estilo. Por ejemplo, con una plantilla para entradas de blog, se puede generar un nuevo archivo HTML por cada archivo Markdown de contenido en la carpeta
content

# Referencia de Markdown

## Código (ancho fijo)

Para escribir una palabra en ancho fijo, se escribe entre acentos graves:
`hugo`
escrito como \`hugo\`.

(Para escribir los acentos sin que se renderice en ancho fijo, se añade el carácter de escape `\`.

---
title: "Página principal"
---

¡Hola! Éste es mi espacio para publicar las cosas que he estado aprendiendo.
A menudo estarán relacionadas con las matemáticas,
y no siempre tendrán una apariencia terminada;
de hecho, preveo publicar cosas a medio terminar, e ir refinándolas con el tiempo,
o incluso cambiándolas si cambia mi opinión.


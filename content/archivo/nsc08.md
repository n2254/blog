---
title: "Cuenta hasta cero - NSC #8"
categories: ["matemáticas"]
tags: ["aritmética modular", "álgebra", "criptología", "fundamentos"]
slug: cuenta-hasta-cero-nsc-8
aliases: [/archivo/2018/06/22/cuenta-hasta-cero-nsc-8/]
date: 2018-06-22T10:44:35+0000
lastmod: 2018-06-22T10:49:55+0000
---

Plantéate el siguiente juego (asumo que aceptas llamar juego a las matemáticas).
Escoge un número natural al azar, distinto de cero: llamémoslo \\(N\\).
Empieza a contar desde cero, como si contases ovejas,
sólo que ahora, cuando llegues a \\(N\\), no dirás \\(N\\) sino 0,
y vuelves a empezar.
Por ejemplo, si \\(N\\) es 5,
empezarías diciendo <em>0, 1, 2, 3, 4</em>,
y volverías a empezar:
<em>0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0</em>...

Reconozco que como juego no es la cosa más entretenida del mundo,
pero tiene una razón de ser.
Hagamos una variante. Recuerda tu número \\(N\\) y empieza a contar desde cero,
pero esta vez no digas el número en sí,
sino el resto que da al dividirlo entre \\(N\\).
Por ejemplo, si \\(N\\) es 5, los números hasta el 4 no cambian;
sin embargo, 5 entre 5 da resto 0, y 6 entre 5 da resto 1, y 7 entre 5 da resto 2;
el resto sigue subiendo hasta que, más adelante,
llegas a que 10 entre 5 da resto 0, y que 11 entre 5 da resto 1, y así.
El juego con el que empezamos resulta ser nada más y nada menos
que dar los restos de los números naturales divididos entre \\(N\\).

De nuevo, esto no parece la revelación más emocionante:
¿a quién narices le importan los restos de las divisiones?
Lo importante es el cociente, ¿no? Sí, y no.
A veces conocer el resto resulta más interesante que el cociente.
De ahí que tengamos palabras específicas para designar un número
según su resto al dividirlo entre 2: <em>par</em> o <em>impar</em>.
A los que practican deportes por equipos de manera informal
(iba a decir "los niños que juegan al fútbol en el recreo",
pero no sé si queda de eso o si están todos pegados al móvil)
les interesa a menudo saber si pueden repartirse en equipos de forma equitativa,
sin que un equipo tenga más jugadores que el otro:
no importa tanto cuántos juegan en cada equipo, sino saber si son pares o no.
En otros ámbitos, el día tiene 1440 minutos distribuidos en 24 horas,
pero a menudo la hora concreta no es tan importante como los minutos:
piensa en quien depende del transporte público
y sabe que el tren "pasa a y doce" a todas horas,
o en los amigos que quedan "a y media" y que se pueden permitir omitir la hora exacta.
Por cierto, que en el tema horario también se ve muy bien
este juego de las listas de números:
tu reloj va marcando minutos hasta que llega a 60,
y luego vuelve a empezar por cero y así una y otra vez.

Hasta aquí, el porqué alguien no relacionado con las matemáticas
no debería despreciar de entrada los restos.
Sin embargo, podemos ver más cosas de interés si no nos dan miedo los números.
Pongamos de nuevo que \\(N=5\\):
tenemos la lista de números {0, 1, 2, 3, 4}
que se repite una y otra vez.
Podemos inventarnos una manera de hacer operaciones con ellos
sin rompernos demasiado la cabeza.
Por ejemplo, para la suma, podemos sumarlos como hacíamos con los naturales,
y si el resultado queda por encima de 4 podemos dividirlo entre 5 y tomar el resto.
Así, 3+4 debería ser 7, pero como queda fuera de la lista,
lo dividimos entre 5, y da resto 2.
Si suena a mucho trabajo (aquí hacemos más cosas que cuentas, ¿no?),
puedes ver la suma también como desplazarte en la lista de números

0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0,...

Ahora, 3+4 es el número que está cuatro posiciones a la derecha de 3;
da igual por qué 3 empieces, siempre llegarás a 2.
Esto se parece también a cómo contamos los minutos en el reloj:
si el reloj marca 56 minutos y esperamos 17, marcará 13,
que es el resto de 56+17=73 entre 60.
Para restar haríamos algo parecido:
restar y tomar el resto, o desplazarnos hacia la izquierda en la lista de restos.
La imagen de desplazarnos por la lista ya no sirve tan bien para multiplicar,
pero usar los restos sí: 4·4=16 en la vida real,
pero en nuestro juego 4·4=1 porque 16 entre 5 da resto 1.

Lo bueno de tener listas cortas de números
es que podemos hacer tablas de sumar y multiplicar para todos los números.
En el caso de \\(N=5\\), las tablas que salen son

<table style="border:0;width:40%;margin-left:30%;margin-right:30%;">
<tr>
<th style="border:0;border-bottom:1px solid;border-right:1px solid;text-align:center;">+</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">0</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">1</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">2</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">3</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">4</th>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">0</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">4</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">1</th>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">0</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">2</th>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">3</th>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">4</th>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
</tr>
</table>

<table style="border:0;width:40%;margin-left:30%;margin-right:30%;">
<tr>
<th style="border:0;border-bottom:1px solid;border-right:1px solid;text-align:center;">·</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">0</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">1</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">2</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">3</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">4</th>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">0</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">1</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">4</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">2</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">3</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">3</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">2</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">4</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">4</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">1</td>
</tr>
</table>

Podemos plantearnos otros dos casos: el caso \\(N=2\\)

<table style="border:0;width:20%;margin-left:40%;margin-right:40%;">
<tr>
<th style="border:0;border-bottom:1px solid;border-right:1px solid;text-align:center;">+</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">0</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">1</th>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">0</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">1</th>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">0</td>
</tr>
</table>

<table style="border:0;width:20%;margin-left:40%;margin-right:40%;">
<tr>
<th style="border:0;border-bottom:1px solid;border-right:1px solid;text-align:center;">·</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">0</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">1</th>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">0</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">1</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
</tr>
</table>

y el caso \\(N=4\\)

<table style="border:0;width:34%;margin-left:33%;margin-right:33%;">
<tr>
<th style="border:0;border-bottom:1px solid;border-right:1px solid;text-align:center;">+</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">0</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">1</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">2</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">3</th>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">0</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">1</th>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">0</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">2</th>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">3</th>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
</tr>
</table>

<table style="border:0;width:34%;margin-left:33%;margin-right:33%;">
<tr>
<th style="border:0;border-bottom:1px solid;border-right:1px solid;text-align:center;">·</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">0</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">1</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">2</th>
<th style="border:0;border-bottom:1px solid;text-align:center;">3</th>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">0</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">0</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">1</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">1</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">3</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">2</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">2</td>
</tr>
<tr>
<th style="border:0;border-right:1px solid;text-align:center;">3</th>
<td style="border:0;text-align:center;">0</td>
<td style="border:0;text-align:center;">3</td>
<td style="border:0;text-align:center;">2</td>
<td style="border:0;text-align:center;">1</td>
</tr>
</table>

Probablemente veas en las tablas de sumar el fenómeno que decía
de desplazarse por la lista de restos:
cada línea es la anterior, pero movida hacia la izquierda una posición.
En cuanto a las tablas de multiplicar,
no te sorprenderá ver que el producto de cero por cualquier número es cero:
tiene sentido, porque si un número es divisible por \\(N\\),
cualquier múltiplo suyo también lo será
(por ejemplo, par por par es par, par por impar es par).
Quizá sí te sorprenda ver que, en el caso de \\(N=5\\),
todo número distinto de cero tiene inverso,
a diferencia de lo que ocurre con los enteros sin más:
en todas las filas se aprecia que hay un producto que da 1.
Lo mismo pasa con \\(N=2\\),
aunque este caso es menos espectacular porque sólo hay un número distinto de cero;
ilustra que multiplicar impar por impar sigue dando impar.
En cambio, en el caso \\(N=4\\),
no todos los números distintos de cero tienen inverso:
si te fijas en el 2, no hay ningún número que multiplicado por 2 dé 1.
De hecho, y esto es lo que puede resultar algo inquietante
si es tu primera vez viendo estos números,
lo que se ve es que 2·2=0:
hay una manera de multiplcar dos números distintos de cero
y que el resultado sea cero.
Y, de nuevo, el resultado tiene sentido:
si multiplicas dos números divisibles entre 2, el resultado será divisible entre 4.

En jerga, cuando dos números distintos de cero dan lugar a un producto igual a cero,
se dice que estos números son <b>divisores de cero</b>.
La existencia de estos divisores de cero no tiene lugar siempre:
en nuestros ejemplos, sólo ocurre en el caso \\(N=4\\).
Sin embargo, no es difícil ver que números así existirán
siempre que tomemos restos donde \\(N\\) es un número compuesto,
es decir, no primo.
De hecho, en los casos donde \\(N\\) es primo,
no sólo se tiene que no existen divisores de cero,
sino que todo número distinto de cero tendrá inverso.

Quería incluir las tablas para \\(N=2\\) porque ya las hemos discutido antes,
al menos una de ellas: en este caso,
la suma es la misma que la que denotamos con \\(\oplus\\) cuando discutimos el one-time pad.
Esto puede ayudar a ver que,
más que una mera curiosidad o juego que me he sacado de la manga,
hay situaciones en las que estas cuentas aparecen
y que hacen que estos conceptos sean interesantes.
Entonces llamamos a la suma \\(\oplus\\) <em>suma módulo 2</em>; en general,
llamamos a estas operaciones <b>suma</b> y <b>producto módulo</b> \\(N\\),
en conjunto <b>aritmética modular</b>,
y al conjunto de restos de dividir entre \\(N\\)
(una vez eliminados los que están repetidos)
donde están estas operaciones lo llamamos <b>anillo de enteros módulo </b>\\(N\\),
denotado \\(\mathbf Z/N\mathbf Z\\).
Es decir, \\(\mathbf Z/N\mathbf Z=\\{0,1,...,N-1\\}\\).
Estos "anillos" tienen un papel bastante importante en criptografía,
y no será la última vez que hablemos de ellos.

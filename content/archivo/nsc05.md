---
title: "Pero algunos son más infinitos que otros - NSC #5"
categories: ["matemáticas"]
tags: ["axioma de elección", "cardinales", "fundamentos", "hipótesis del continuo", "ordinales", "teoría de conjuntos"]
slug: pero-algunos-son-mas-infinitos-que-otros-nsc-5
aliases: [/archivo/2018/03/26/pero-algunos-son-mas-infinitos-que-otros-nsc-5/]
date: 2018-03-26T10:53:07+0000
lastmod: 2018-03-26T10:53:07+0000
---

¿Hay más números pares, impares, o naturales?
Parece que debería haber tantos pares como impares,
porque a cada impar puedes asociarle un par (súmale o réstale 1),
lo que implica que hay una biyección entre ellos y,
como vimos en la entrada anterior, que tienen el mismo cardinal
(o sea, "la misma cantidad de elementos").
Ahora, en su día, yo creía que habría más naturales que pares,
porque los naturales contienen todos los pares pero tienen además números extra.
En la jerga matemática,
había encontrado una función inyectiva de los pares a los naturales
que no era suprayectiva, luego no era una biyección.
Pero bien podía ser que existiera otra función que sí lo fuera.
Y, en efecto, existe: la función que multiplica por 2 cada número natural
es una función de los naturales a los pares,
que es inyectiva
(no puede ser que dos números distintos sean iguales tras multiplicarlos por 2)
y suprayectiva (cualquier par es el doble de otro número natural).
Luego, por sorprendente que pueda parecer en un principio,
hay tantos pares como naturales.

Esta pregunta se puede repetir para otros sistemas de numeración.
Igual que los naturales contienen a los pares
pero tienen el mismo cardinal que ellos,
podrías preguntarte si conjuntos que contengan a los naturales
tienen el mismo cardinal que estos últimos.
Por ejemplo, los enteros, representados con el símbolo \\(\mathbf Z\\)
y que vienen a ser los naturales con signo '+' o '-'.
En este caso, la situación recuerda bastante a la de los naturales y los pares;
de hecho, si a cada natural par le asocias un entero positivo,
y a cada natural impar un entero negativo,
es posible encontrar una biyección,
y por ende demostrar que \\(\mathbf N\\) y \\(\mathbf Z\\) tienen el mismo cardinal.
Esto no es difícil, así que te dejo los detalles a ti.

Mientras otro caso: el de los racionales, \\(\mathbf Q\\),
que vienen a ser fracciones de enteros.
Se puede demostrar que \\(\mathbf Q\\) tiene el mismo cardinal que \\(\mathbf N\\)
y, por tanto, también que \\(\mathbf Z\\).
Para ello, fíjate en esto:
si encontramos una biyección
entre los racionales <em>positivos</em> y los naturales,
sabremos que existe una biyección
entre los enteros y los racionales (¿ves por qué?).
Así que basta encontrar una biyección
entre los naturales y las fracciones positivas.
Ahora bien, una fracción positiva viene a ser lo mismo
que un par de números naturales, siempre y cuando el de abajo no sea 0.
De hecho, como las fracciones con un 0 arriba son todas iguales,
podemos suponer que nuestra biyección manda el natural 0 a las fracciones con 0 arriba,
y preocuparnos sólo de encontrar una correspondencia
entre naturales mayores que 0 y fracciones mayores que 0.
Si hacemos una tabla según los números que ponemos arriba y abajo,

<table style="border:0;">
<tr>
<td style="border:0;width:20px;">
1/1
</td>
<td style="border:0;width:20px;">
1/2
</td>
<td style="border:0;width:20px;">
1/3
</td>
<td style="border:0;width:20px;">
1/4
</td>
<td style="border:0;width:20px;">
1/5
</td>
<td style="border:0;width:200px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
2/1
</td>
<td style="border:0;width:50px;">
2/2
</td>
<td style="border:0;width:50px;">
2/3
</td>
<td style="border:0;width:50px;">
2/4
</td>
<td style="border:0;width:50px;">
2/5
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
3/1
</td>
<td style="border:0;width:50px;">
3/2
</td>
<td style="border:0;width:50px;">
3/3
</td>
<td style="border:0;width:50px;">
3/4
</td>
<td style="border:0;width:50px;">
3/5
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
4/1
</td>
<td style="border:0;width:50px;">
4/2
</td>
<td style="border:0;width:50px;">
4/3
</td>
<td style="border:0;width:50px;">
4/4
</td>
<td style="border:0;width:50px;">
4/5
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
5/1
</td>
<td style="border:0;width:50px;">
5/2
</td>
<td style="border:0;width:50px;">
5/3
</td>
<td style="border:0;width:50px;">
5/4
</td>
<td style="border:0;width:50px;">
5/5
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
...
</td>
</tr>
</table>

podemos hacer un truco como el que hicimos para ver
que \\(ω\\) y \\(ω^2\\) tenían el mismo cardinal:
podemos asociar el 1 a 1/1, el 2 a 1/2, el 3 a 2/1,
y seguir las diagonales para el resto de números.
Hay un pequeño problema que no teníamos con \\(ω^2\\),
y es que algunos de estos números se estarían contando dos veces:
por ejemplo, 1/1 representa el mismo número racional que 2/2.
Pero se puede arreglar si cogemos solamente
la fracción con los números más pequeños,
y nos saltamos las otras fracciones cuando aparezcan.
El resultado de la asociación es el siguiente:

<table style="border:0;">
<tr>
<td style="border:0;width:20px;">
1
</td>
<td style="border:0;width:20px;">
2
</td>
<td style="border:0;width:20px;">
4
</td>
<td style="border:0;width:20px;">
6
</td>
<td style="border:0;width:20px;">
10
</td>
<td style="border:0;width:200px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
3
</td>
<td style="border:0;width:50px;">
-
</td>
<td style="border:0;width:50px;">
7
</td>
<td style="border:0;width:50px;">
-
</td>
<td style="border:0;width:50px;">
13
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
5
</td>
<td style="border:0;width:50px;">
8
</td>
<td style="border:0;width:50px;">
-
</td>
<td style="border:0;width:50px;">
14
</td>
<td style="border:0;width:50px;">
19
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
9
</td>
<td style="border:0;width:50px;">
-
</td>
<td style="border:0;width:50px;">
15
</td>
<td style="border:0;width:50px;">
-
</td>
<td style="border:0;width:50px;">
24
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
11
</td>
<td style="border:0;width:50px;">
16
</td>
<td style="border:0;width:50px;">
20
</td>
<td style="border:0;width:50px;">
25
</td>
<td style="border:0;width:50px;">
-
</td>
<td style="border:0;width:50px;">
...
</td>
</tr>
<tr>
<td style="border:0;width:50px;">
...
</td>
</tr>
</table>

Te dejo, de nuevo, que tú sola compruebes los detalles,
porque lo interesante viene ahora.
Podemos definir otro conjunto por encima de los racionales,
a saber, el de los números reales, o \\(\mathbf R\\).
Aquí entran todos los números que se pueden escribir con decimales,
tengan la cantidad de decimales que tengan.
Los naturales y enteros se pueden escribir siempre sin decimales,
así que entran en los reales;
las fracciones se pueden escribir con decimales,
algunas con finitos decimales,
y otras con listas infinitas de decimales
pero en las que hay una cantidad finita que se repite periódicamente.
Desde luego, hay reales que no son racionales
(miremos a \\(π\\), sin ir más lejos, con sus infinitos decimales),
así que una pregunta razonable es si hay más reales que racionales
o si, de forma semejante a las discusiones anteriores,
\\(\mathbf Q\\) y \\(\mathbf R\\) tienen el mismo cardinal.
La respuesta a esta pregunta es: \\(\mathbf Q\\) y \\(\mathbf R\\)
<b>no</b> tienen el mismo cardinal, y lo formulamos de la siguiente manera:

<p style="text-align:left;"><b>TEOREMA DE CANTOR</b>
Cualquier función de \\(\mathbf N\\) a \\(\mathbf R\\) es necesariamente no suprayectiva.</p>

Y, como intentamos ser serios, lo demostramos.

<b><em>Demostración.</em></b>
Supongamos que \\(f\\) es una función
de los números naturales a los números reales;
habremos demostrado lo que queremos si encontramos
un número real distinto de todos los \\(f(n)\\),
donde \\(n\\) es un número natural.
Aunque la idea funciona en general
(y debe hacerlo: si no, no es una demostración),
es conveniente ilustrarla con un ejemplo.
Supongamos que los valores de \\(f\\) son los que aparecen en esta tabla:

<table style="border:0;">
<tr>
<td style="border:0;width:100px;">
<em>n</em>
</td>
<td style="border:0;">
<em>f(n)</em>
</td>
</tr>
<tr>
<td style="border:0;width:100px;">
0
</td>
<td style="border:0;">
12,57239...
</td>
</tr><tr>
<td style="border:0;width:100px;">
1
</td>
<td style="border:0;">
-5,19109...
</td>
</tr><tr>
<td style="border:0;width:100px;">
2
</td>
<td style="border:0;">
0,252525...
</td>
</tr><tr>
<td style="border:0;width:100px;">
3
</td>
<td style="border:0;">
1587,234...
</td>
</tr>
<tr>
<td style="border:0;width:100px;">
4
</td>
<td style="border:0;">
3,141592...
</td>
</tr><tr>
<td style="border:0;width:100px;">
5
</td>
<td style="border:0;">
6,283105...
</td>
</tr>
<tr>
<td style="border:0;width:100px;">
<em>...</em>
</td>
<td style="border:0;">
<em>...</em>
</td>
</tr>
</table>

Queremos construir un número real \\(x\\)
que no aparezca en la columna de la derecha.
En particular, queremos que \\(x\\) sea distinto de \\(f(0)\\),
así que tomamos la parte entera de \\(x\\)
(lo que está a la izquierda de la coma)
como distinta de \\(f(0)\\);
por ejemplo, \\(x=8,...\\).
También queremos que sea distinto de \\(f(1)\\),
así que podemos poner la primera cifra decimal de \\(x\\)
de forma que sea distinta a la de \\(f(1)\\);
por ejemplo, si \\(x=8.3...\\).
Y que sea distinto de \\(f(2)\\),
haciendo que la segunda cifra decimal sea distinta de la de \\(f(2)\\);
por ejemplo, \\(x=8.39...\\).
Y que sea distinto de \\(f(3)\\),
haciendo que la tercera cifra decimal sea distinta de la de \\(f(3)\\);
por ejemplo, \\(x=8.391...\\).
Podemos seguir así eternamente:
si tenemos un número \\(x\\) que es distinto de todos los \\(f(n)\\)
para \\(n\\) por debajo de una cota determinada \\(N\\),
podemos obtener un nuevo \\(x\\) distinto de todos los \\(f(n)\\)
para \\(n\\) por debajo de \\(N+1\\),
simplemente añadiendo al final un decimal distinto del de \\(f(N+1)\\).
Esto nos permite concluir que debe existir un número real \\(x\\)
que es distinto de todos los números \\(f(n)\\);
hemos encontrado lo que queríamos.

<hr>

Y así acaba la primera demostración seria que hacemos en el blog.
Espero que estés satisfecha con el resultado,
porque ahora quiero comentar la consecuencia que tiene esto.
Como \\(\mathbf N\\) y \\(\mathbf R\\) no tienen el mismo cardinal,
pero \\(\mathbf R\\) es claramente infinito,
cualquier ordinal con el que \\(\mathbf R\\)
debe estar por encima de todos los que habíamos visto hasta ahora.

No pequeño pero relevante inciso,
apropiado si tienes gran curiosidad o si lo has entendido todo hasta ahora.
Sabemos que, si tenemos un ordinal, siempre existe un ordinal por encima;
lo que no sabemos es que exista un ordinal infinito
que no sea biyectivo con \\(ω\\), es decir, con \\(\mathbf N\\).
Gracias a este teorema, sabemos que \\(\mathbf R\\) no es biyectivo con \\(ω\\),
pero no sabemos si \\(\mathbf R\\) es biyectivo con algún ordinal.
Como queremos poder hablar de la "cantidad de elementos" de \\(\mathbf R\\),
nos conviene que exista un ordinal así,
pero no podemos asegurar, al menos no ahora, que exista.
¿Qué hacemos? Lo mismo que cuando nos convenía que existieran conjuntos infinitos:
estipular por axioma que exista. Para esto usamos el

<p><b>TEOREMA DE BUENA ORDENACIÓN DE ZERMELO</b>
Para todo conjunto, existe un ordinal con el que es biyectivo.</p>

Ahorrarnos los problemas a golpe de axioma puede parecer un tanto cutre,
pero en este caso es necesario.
Este axioma se llama "teorema" porque se puede demostrar a partir de otro axioma,
el llamado <b>axioma de elección</b>,
pero como es tal vez demasiado abstracto para introducirlo aquí,
prefiero considerar el teorema de Zermelo como un axioma
(de hecho, el axioma de elección se puede demostrar a partir del teorema de Zermelo,
haciéndolos equivalentes y justificando que llame axioma a este último).
Siendo completamente estrictos,
el teorema de Zermelo no debería formularse así
sin haber hablado un poco más de ordinales y conjuntos bien ordenados en general,
pero de nuevo esto me parece algo más accesible.

El caso es que, gracias al teorema de Zermelo,
sabemos que debe existir un ordinal con el que \\(\mathbf R\\) sea inyectivo.
Como el conjunto de los números reales es infinito,
este ordinal también debe serlo;
pero gracias al teorema de Cantor,
este ordinal no puede ser biyectivo con \\(ω\\).
¡Luego existen ordinales tan grandes
que tienen cardinal mayor que el de \\(ω\\)!
Hemos encontrado un infinito que es, realmente,
más grande que el anterior infinito que conocíamos.
El hallazgo no es trivial,
y ha sido necesario traer la maquinaria del teorema de Zermelo
y demostrar el teorema de Cantor para ello.
Pero lo hemos conseguido, y podemos estar orgullosos.

Dos preguntas ahora se hacen relevantes:
¿cuántos cardinales hay así?,
y ¿es el cardinal de \\(\mathbf R\\) el cardinal más pequeño por encima de \\(ω\\)?
La respuesta a la primera pregunta es <em>infinitos</em>:
existe una versión más fuerte del teorema de Cantor
que muestra que, para todo conjunto \\(A\\),
existe un conjunto \\(B\\) tal que
cualquier función de \\(A\\) a \\(B\\) no es suprayectiva.
De nuevo, gracias a Zermelo, esto muestra que deben tener distinto cardinal;
volviendo a aplicar el teorema una y otra vez nos da cardinales mayores y mayores.
Lo gracioso es que estos cardinales se suelen numerar,
haciendo una especie de lista:
el cardinal infinito más pequeño, que hasta ahora denotábamos por \\(ω\\),
se denota en esta lista como \\(\aleph_0\\);
el siguiente (o sea, el más pequeño que sigue siendo más grande que \\(ω\\))
se denota \\(\aleph_1\\);
el siguiente, \\(\aleph_2\\), y así.
Existe incluso un \\(\aleph_\omega\\), e incluso mayores.
Siempre se pueden construir cardinales mayores.

A la segunda pregunta, la respuesta es complicada.
La pregunta de si el cardinal de \\(\mathbf R\\)
es el cardinal que se denota por \\(\aleph_1\\)
tiene el interesante nombre de <b>hipótesis del continuo</b>,
y constituye uno de los mejores ejemplos
de lo antiintuitivas que pueden ser las matemáticas y la lógica
a este nivel fundamental.
Y es que se ha demostrado lo siguiente:
no se puede demostrar que la hipótesis del continuo es falsa,
y no se puede demostrar que es cierta.
Para los matemáticos, puede tener sentido en contextos distintos
que la hipótesis del continuo sea cierta y que sea falsa
Pero afirmar tajantemente que el cardinal de \\(\mathbf R\\)
es o no \\(\aleph_1\\) no es válido.
En jerga: hay <em>modelos</em> de la teoría de conjuntos
donde la hipótesis del continuo es cierta,
y hay modelos en los que es falsa.
Pero esto ya nos lleva por otros derroteros.
Creo que hemos tratado bastante (por el momento)
el tema de los infinitos en la teoría de conjuntos,
y me apetece que la próxima vez estudiemos otros asuntos.
Tal vez más adelante merezca la pena volver por aquí, pero no por un tiempo.
Salvo que tengas tremendas ganas, claro;
a fin de cuentas, estoy aquí para explicarte cosas que puedan ser de interés para ti.

---
title: "Ordenando hasta el infinito - NSC #2"
categories: ["matemáticas"]
tags: ["fundamentos", "ordinales", "teoría de conjuntos"]
slug: ordenando-hasta-el-infinito-nsc-2
aliases: [/archivo/2018/02/25/ordenando-hasta-el-infinito-nsc-2/]
mathjax: true
date: 2018-02-25T13:30:07+0000
lastmod: 2018-02-25T13:30:07+0000
---

En la última entrada habíamos tratado
cómo escribir los números desde el punto de vista de conjuntos:
el 0 sería el conjunto vacío, sin elementos;
el 1, el conjunto que contiene únicamente al número 0;
el 2, el conjunto que contiene a 0 y 1, y así.
Esto nos daba una forma de describir cualquier número, en principio:
empezando desde el 0, podíamos ir describiéndolos todos de uno en uno
hasta llegar al número deseado.
Cierto es que, si el número que te interesa describir es gigantesco,
tal vez no llegues a él antes de que la muerte llegue a ti,
pero en este momento,
con nuestro sombrero de matemático puro sobre nuestras cabezas,
cómo hacer las cosas en la práctica nos parece irrelevante.

Otra cosa que había comentado es
cómo esta noción de número se lleva bastante bien
con las nociones que tenemos acerca de cantidad y orden.
Cantidad, dijimos,
porque el número 42 será un conjunto de exactamente cuarenta y dos elementos;
orden, porque para definir 42 antes debía haber sido definido el 41:
el orden en el que definimos los números sigue el orden creciente de éstos.

Ahora, tienen una limitación,
y es que estas cantidades y órdenes siempre han de ser finitos,
igual que los números mismos.
Sin embargo, uno podría pensar que
el conjunto de todos los números naturales,
que normalmente se denota como \\(\mathbf{N}\\) o \\(\mathbb{N}\\),
podría incluirse también en ese orden de los números:
a fin de cuentas, necesita que todos los números se hayan definido antes,
así que podría corresponder a un "número" mayor que todos los demás números.
Algo que encaja con la idea que tenemos de *infinito*.

Lamentablemente, hay un ligero problema con esto.
Si quieres dar una descripción exhaustiva de un número enorme,
dando la lista de todos los números menores que él,
podrías hacerlo en una cantidad de tiempo finita,
lo que, para matemáticos puros como nosotros,
significa que es una tarea perfectamente realizable.
Pero el conjunto de los números naturales es infinito:
describir todos los números no es algo
que se pueda hacer uno a uno en una cantidad de tiempo finita,
por lo que tal conjunto no se puede construir de la misma forma
que cada uno de los números que lo componen.
Hemos llegado a un punto en el que es necesario invocar
una de las palabras más tradicionalmente asociadas a las matemáticas: los *axiomas*.
Ante la imposibilidad de construir
un conjunto infinito de una manera finita,
pero la necesidad de manipular cosas en matemáticas que son infinitas,
nos vemos obligados a introducir
la existencia de estos conjuntos como axioma,
como hecho que no requiere demostración.
Formulamos así el Axioma de Infinitud,
o al menos una versión que sea suficiente para nuestros propósitos:

{{% center %}}
**AXIOMA DE INFINITUD** (versión NSC)\
El conjunto de los números naturales \\(\mathbf{N}\\) existe.
{{% /center %}}

Bien. Sabemos ahora que \\(\mathbf{N}\\) existe,
aunque no tengamos una manera de construirlo.
A partir de aquí, podemos ya continuar nuestros asuntos de órdenes.
Por lo que a nosotros respecta, \\(\mathbf{N}\\) se comporta
como un número que es mayor que cualquier otro número natural.
Es interesante aquí cambiar la notación para reflejar este hecho:
ya que el conjunto de números naturales \\(\mathbf{N}\\)
pasa a ser un número más,
lo podemos representar con una letra minúscula;
para indicar que está al final de todos los demás números,
podemos elegir la letra griega omega.
Así, acabamos con una extraña lista de "números":

{{% center %}}
0, 1, 2, 3, ..., 42, ..., 1024, ..., 196883, ... ... ... ... ... \\(\omega\\)
{{% /center %}}

Te dejo que medites sobre esto, si lo necesitas.
Acabamos de hacer algo que suena a prohibido:
*hemos alcanzado el infinito*,
le hemos dado un significado dentro de nuestro sistema habitual de numeración,
que claramente excluía cualquier cosa no finita.
Más aún, tiene el sentido que debía tener:
el infinito es *mayor* que cualquier otro número, como cabía esperar.
Y le hemos dado un final a nuestra lista de números,
incómodamente incompleta por tener un principio y no un final...

Ya. Respecto a eso último.
Hemos llegado *hasta el infinito*,
pero si has visto Toy Story de pequeño (o de no tan pequeño)
posiblemente estés pensando en qué hay *más allá*.
Hasta ahora, siempre que teníamos un número *n*,
podíamos formar un número *n*+1 que estaba justo por encima de *n*:
simplemente, construíamos el conjunto que contiene
a todo lo que contenía *n* (es decir, a los números por debajo de *n*),
más al propio número *n*.
Y, en esta situación en que nos hallamos,
tenemos un "número" *ω* que contiene
a todos los números más pequeños que él,
pero obviamente no a sí mismo
(sería raro: imagínate tener que definir *ω* para poder definir *ω*).
Así que parece que hay un hueco en el mercado
para un "número" que contenga a todos los números naturales Y a *ω*.
O sea, algo superior a *ω*.
Y, no sé tú, pero si a mí de pequeño me preguntan que va después de infinito,
mi respuesta está clara: después de infinito va *infinito más uno*.
Y ¿qué es un matemático sino un niño pequeño
(que juega a escribir cosas sin sentido para el resto de los mortales)?
Definimos

\\[
\omega+1:= \\{0, 1, 2, 3, 4, 5, 6, \ldots\\, \ldots\\, \ldots\\, \ldots\\, \ldots\\, \omega\\}
\\]

Si tu traumática educación matemática te impele
a rebatir que tal cosa pueda existir, piensa un momento.
La manera de definir *ω*+1 es perfectamente válida
desde el momento que admitimos que algo como *ω* pueda tener sentido,
lo que sale del Axioma de Infinitud.
Hay circunstancias en las que sumar 1 a infinito
se puede (y debe) ver como el mismo infinito,
pero ahora mismo esto no nos concierne:
lo que *ω*+1 representa no es una cantidad infinita de cosas,
a la que añadir una cosa más no debería afectar,
sino un objeto que,
desde el punto de vista del orden que hemos usado en esta entrada,
va *después* que *ω*+1.
Tal vez ahora entiendas por qué insisto en indicar
que el orden de mayor/menor se puede ver como después/antes:
para cosas infinitas,
donde hablar de infinitos mayores que otros infinitos nos cuesta algo de trabajo,
aún podemos hablar de ese orden, digamos, secuencial:
qué infinito se define después que el otro.


Si has aceptado la existencia de *ω*+1 en nuestras vidas,
deberías estar preparado para aceptar algo como *ω*+1+1,
normalmente denotado como *ω*+2:
aquel infinito que va detrás de *ω*+1.
Y podrás aceptar también que nada impide
que podamos hablar de *ω*+*n* para cualquier número natural *n*,
construyéndolo a partir de *ω* igual que construíamos *n* a partir de 0.
Incluso, puedes llegar a ver cómo nos encontraríamos
con el mismo problema que llevó a la definición de *ω*;
en este caso: ¿qué "número" iría después de todos los *ω*+*n*?
Bueno, igual que antes rematamos la secuencia de números con un *ω*,
podemos hacer ahora algo parecido y rematar la lista de *ω*+*n* con un *ω*+*ω*.
Denotándolo de una forma similar a lo que hacemos con los números, tendríamos

\\[
\omega\cdot2:=\\{0, 1, 2, \ldots\\, \omega,\\, \omega+1,\\, \omega+2,\\, \ldots\\, \\}
\\]

Y podemos seguir: ¿qué impide ahora seguir incrementando esta lista con *ω*·2+1?
Podríamos llegar incluso a algo como *ω*·3. Y a *ω*·4.
Y llegar a un mega infinito como *ω*·*ω*.
Pero espera, esto se denota como *ω*<sup>2</sup>, ¿no?
Y, tras una escalada febril de entusiasmo ordinal,
llegar a que tu lista de "números" se parece a

\\[
\begin{align*}
0,\\, 1,\\, \ldots,\\,
&\omega,\\; \omega+1,\\, \ldots,\\,
\omega\cdot2,\\;\\; \omega\cdot2+1,\\; \ldots, \\\\
&\omega^2,\\; \omega^2+1,\\, \ldots,\\,
\omega^2+\omega,\\;\\; \omega^2+\omega+1,\\, \ldots, \\\\
&\omega^3, \ldots,\\, \omega^\omega, \ldots,\\, \omega^{\omega^\omega}, \ldots
\end{align*}
\\]

Y esta vez puedes concluir que esta lista no puede acabarse nunca:
siempre podrás seguir sumando uno más.
Claro que eso no nos impidió definir *ω*
para la primera lista de números naturales,
inofensivos pero que no se acababan nunca tampoco...
Guardémonos este pensamiento para la próxima vez
que nos pongamos nuestro sombrero de matemático puro.

Imagino que tu mente andará perdida entre omegas sin fin,
pero hay una última cosa que quiero decirte.
Esta entrada ha tratado de cómo se ordenan estos "números",
tanto finitos como infinitos;
es un tema que tiene su relevancia en esta parte fundamental de las matemáticas,
y conviene introducir alguna manera
de referirnos a estos "números" sin usar comillas.
La palabra clave se me ha escapado antes: los llamamos <b>ordinales</b>,
reflejando esta noción de orden tan central para su definición.
La definición rigurosa de qué es un ordinal puede quedar para más adelante,
si es necesario; de todas formas,
si de ahora en adelante vuelven a aparecer estos "números",
probablemente los llame ordinales.

Creo que es más que suficiente por hoy. La próxima vez, seguiremos en el infinito.


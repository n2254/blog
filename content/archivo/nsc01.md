---
title: "Aprendiendo los números - NSC #1"
categories: ["matemáticas"]
tags: ["fundmentos", "números", "teoría de conjuntos"]
slug: aprendiendo-los-numeros-nsc-1
aliases: [/archivo/2018/02/18/aprendiendo-los-numeros-nsc-1/]
date: 2018-02-18T13:50:27+0000
lastmod: 2018-03-14T12:16:39+0000
---

¿Qué es un número? Es una pregunta razonable.
Todos sabemos que tres manzanas no son lo mismo
que tres ovejas o que tres ciudades,
pero no obstante tenemos una idea
de que hay algo conectando esas nociones.
Que, desde el punto de vista de la *cantidad de cosas*,
esas tres nociones son muy parecidas.
Tan parecidas que somos capaces de abstraer ese algo común,
esa noción de *tres*, denotarla con el símbolo 3
y jugar con la idea abstracta en clases de matemáticas.
Te puedes preguntar, entonces,
qué hay de real en esa idea abstracta, en ese 3 y,
en tu búsqueda de respuestas, acudir a este aún joven blog.

Si es así, sigue buscando, porque no te voy a dar una respuesta. No a esa pregunta formulada de esa manera, al menos. Aunque es un tema tremendamente interesante, no me veo capacitado para abordarlo en esta fase del blog. Pero sí puedo aportar otra cosa: una posible *definición* de los números. Algo que no satisfará tus inquietudes filosóficas, pero que al menos nos dará algo desde lo que fundamentar otras ideas matemáticas. Porque, aunque al final del día no sepamos qué es exactamente un número, tal vez podamos saber cómo usarlos para nuestros propósitos. No voy a ser tremendamente formal al respecto; sólo dar un par de ideas que puedan ayudar a entender una definición de los números usada en matemáticas.

Para ello, te sugiero que vuelvas a pensar en tus tres manzanas, ovejas y ciudades. Sabes que existe esa idea común de *tres*, aunque las tres manzanas sean distintas de las tres ovejas. Es precisamente esto lo que hace que no tengamos del todo claro cómo definir la idea de *tres*: si las tres manzanas fueran lo mismo que las tres ovejas, que las tres ciudades y que cualquier otra cosa de la que hubiera tres, decir *tres*, por sí sólo, tendría sentido, no habría ambigüedad. Pero eso no ocurre.

Ahora, ¿qué ocurre si sustituyes *tres* por *cero*? O sea, si piensas en cero manzanas, cero ovejas y cero ciudades. No sólo recuperamos ahora una idea común de *cero*, sino que, en cierto modo, esas cero manzanas sí son lo mismo que esas cero ovejas: *nada*. Si ahora decimos *cero* sí podemos entender cualquier cosa de la que haya cero: todas son la nada, por lo que no hay ambigüedad entre decidir si nos referimos a cero manzanas o a cero ovejas. Ahora sí tenemos una definición para el concepto de *cero*: esa nada.

Tal vez te hayas encontrado en matemáticas con la noción de *conjunto vacío*,
que podemos entender como la manera que tienen los matemáticos
de referirse a *nada*; en tal caso,
habrás visto el símbolo usado para referirse a ello: \\(\emptyset\\).
A la noción de *cero* gustamos de denotarla con el símbolo 0;
si decimos que son la misma cosa,
resulta que tenemos dos símbolos para la misma entidad:
uno para cuando pensemos en el número cero,
y otro para cuando pensemos en el conjunto vacío.
En esta notación, díriamos que \\(0:=\emptyset\\).

Tratemos ahora de definir el *uno*. Como ocurría con el *tres*, si nos limitamos a decir que es una idea común a todas las cosas de las que haya uno, habría una cierta ambigüedad, puesto que una persona podría pensar que *uno* es una manzana y otra que *uno* es una oveja. Necesitaríamos algo para ponernos de acuerdo: algo de lo que sólo haya uno y que tenga un cierto sentido usar de referente (por ejemplo, sería poco propio de un matemático, creo, definir el *uno* como una oveja). Por suerte, hay algo de lo que seguro tenemos uno: tenemos un número, el *cero*. Así que podemos decir que el *uno* es el conjunto formado por el número 0. En notación matemática, \\(1:=\left\\{0\right\\}\\).

Tal vez esto sea un poco confuso, porque empiezo a usar la palabra "conjunto" alegremente. A efectos de esta entrada, un conjunto es una lista de objectos. Cuando hablabas de tres manzanas, puedes pensar en una lista con tres entradas, cada una de ellas una manzana. O, cuando tratamos el 0, la lista que tendríamos estaría vacía, sin entradas. En este caso, lo que propongo es que el 1 sea la lista con una entrada: "0". No sería difícil dar los siguientes pasos ahora: como tenemos dos números, podemos definir el 2 como la lista con dos entradas: "0" y "1". El 3 sería la lista con tres entradas: "0", "1" y "2". Y así.

<hr />

<h4 style="text-align:center;">Lista 0:</h4>
<p style="text-align:center;">(vacía)</p>

<h4 style="text-align:center;">Lista 1:</h4>
<ul style="width:200px;margin:0 auto;text-align:left;">
	<li style="text-align:center;">0</li>
</ul>
<h4 style="text-align:center;">Lista 2:</h4>
<ul style="width:200px;margin:0 auto;text-align:left;">
	<li style="text-align:center;">0</li>
	<li style="text-align:center;">1</li>
</ul>
<h4 style="text-align:center;">Lista 3:</h4>
<ul style="width:200px;margin:0 auto;text-align:left;">
	<li style="text-align:center;">0</li>
	<li style="text-align:center;">1</li>
	<li style="text-align:center;">2</li>
</ul>

<hr />

Que sí, que la idea es rebuscada. Que todo el mundo sabe lo que es 3, aunque sea de forma intuitiva, y que esto no hace demasiada falta. El tema es que, si vives anclada en tu intuición, tal vez no llegues lejos en matemáticas. No serías la primera en formular conjeturas acerca de números, u otros objetos como los que trataremos más adelante, que resultan ser completamente falsas. Ocurre como en las ciencias: allí, da igual lo bonita, intuitiva, sencilla o no, que sea una teoría; si no coincide con lo que dicen los experimentos, no vale nada. En matemáticas, da igual que tu intuición te diga una cosa si la lógica dice otra. Hilbert creía que se podían resumir todas las matemáticas con un par (no literalmente) de axiomas: era a lo que todo lo descubierto apuntaba, así que parecía razonable; y, sin embargo, ahora sabemos que eso no es posible.

Existe una razón para introducir los números de esta forma, aunque no parezca intuitiva. Varias razones, si prefieres. Primero, representar un número de esta forma refleja esa idea de *cantidad* que todos tenemos: el 3 es un conjunto de tres cosas, el 4 de cuatro, y el 0 de nada. Ya que sabemos que en matemáticas se tratan a veces cantidades infinitas, podemos intentar buscar una manera de usar esta imagen que tenemos de las cantidades finitas para entender mejor los infinitos.

Segundo, esta representación refleja una jerarquía u *orden* de los números. Nada en las dos curvas y el pico del número 3 refleja que deba ser mayor que 2 y menor que 4. Sabemos que tres manzanas son menos que cuatro manzanas porque un grupo de cuatro manzanas *contiene* un grupo de tres manzanas; aquí, si vemos los números como listas, la lista del 4 *contiene* como una entrada a la lista del 3. O, de otra forma, para poder definir el 4 has de definir *antes* el 3, estableciendo un orden entre los números: un número es *mayor* que otro si lo *contiene* o si debe definirse *después* que este último. De nuevo, el orden nos es ya muy familiar en casos finitos, pero aparecerán cosas algo más novedosas cuando tratemos infinitos.

Hablando de infinitos. Recalco que todo lo que hemos hecho hasta ahora era con números, bastante finitos. Pero sabemos (intuitivamente, pero aquí la intuición acierta) que hay una cantidad infinita de números. ¿Cómo? Si hubiera un número finito de números, tendría que haber un último número, *N*. Este *N* sería la lista de todos los números anteriores a él, como hemos dicho. Pero el método por el que hemos construido 1 a partir de 0, 2 a partir de 1, etcétera, se generaliza a cualquier número. Es decir, que a partir de ese último número *N* podríamos construir su *sucesor*, convenientemente denotado *N+1.* Como *N* va después que todos los demás (o sea, que todos los otros números salen en la lista de *N),* y *N+1* es un número, debería salir en la lista de *N* también. Pero para definir *N+1* teníamos que definir *N* primero, así que no puede aparecer en la lista de *N.* Hay dos opciones: o *N+1* no es un número, lo que no tiene sentido sabiendo lo que sabemos de los números, o realmente no hay una cantidad finita de números.

O sea, que hay infinitos números. Una cantidad infinita de ellos. Y la pregunta con la que espero que estés entretenida hasta la próxima es: ¿hay un *número* infinito de ellos? Queriendo decir, ¿puedes construir, igual que construimos todos los demás números, ese número infinito? Que te diviertas.

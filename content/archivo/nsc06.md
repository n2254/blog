---
title: "Sin que sepas lo que digo - NSC #6"
categories: ["matemáticas"]
tags: ["criptografía-clasica", "criptología"]
slug: sin-que-sepas-lo-que-digo-nsc-6
aliases: [/archivo/2018/04/21/sin-que-sepas-lo-que-digo-nsc-6/]
date: 2018-04-21T10:56:17+0000
lastmod: 2018-04-21T11:03:32+0000
---

Después de tratar los infinitos, tal vez te hayas hecho consciente (si no lo eras ya)
de que la gente en matemáticas estudia algo más que cómo hacer cuentas,
y que en algunos casos sus objetos de estudio son altamente abstractos y, en fin, raros.
La estrategia que siguen aquellos que intentan vender las matemáticas como algo útil
no pasa, como comprenderás, por hablar de los distintos infinitos que puede haber.
Por el contrario, esta gente te pondrá ejemplos
de campos relacionados con las matemáticas aplicadas.
Te hablarán de análisis de datos, <em>machine learning</em>, cálculo numérico,
reducción de ruido en imágenes o sonido, modelos de desarrollo de tumores, y otras cosas.
Y algo que nunca falta es la criptografía.

El objetivo de la <b>criptografía</b> es, para que nos entendamos,
transformar un mensaje que quieres comunicar a alguien
de forma que dicho mensaje sea ininteligible para todo el mundo
salvo para su destinatario.
Por introducir algo de jerga, el mensaje original se denomina <b>texto plano</b>,
y el transformado es <b>texto cifrado</b>,
y las operaciones de transformar el texto plano en texto cifrado y viceversa
se denominan <b>cifrar</b> o <b>encriptar</b> y <b>descifrar</b>, respectivamente.
La necesidad de tener procedimientos de cifrado que te permitan hacer esto
no debería sonar extraña a nadie.
Los estudiantes aburridos en una clase siempre han buscado poder comunicarse
con sus compañeros para pasar el rato, o hablar de cosas más interesantes,
y en muchos casos les interesa que gente como el profesor pertinente
no se entere de lo que dicen.
Un cliente de un banco posee información que le acredita como propietario
de una cierta cantidad de dinero;
para poder manipular este dinero,
el cliente debe intercambiar esa información con el banco
para demostrar que es el legítimo propietario,
pero no le interesa que nadie más obtenga esa información
y pueda hacerse pasar por él y controlar su dinero.
Pero donde la criptografía es esencial es en la guerra.
Cuando el poder esconder información del enemigo puede ayudar a salvar vidas
(al menos, las del bando amigo, que a menudo parecen ser las únicas de importancia)
y a conseguir grandes objetivos,
grandes recursos se destinan a buscar maneras de intercambiar información de forma segura;
también a buscar maneras de anular los métodos del enemigo
y espiar con éxito sus comunicaciones, lo que se conoce como <b>criptoanálisis</b>.

Pongamos unos ejemplos de cómo esconder información.
Supongamos que necesitas decirle a alguien cuál es tu blog favorito,
pero por algún motivo no quieres que nadie más se entere.
Tu mensaje en texto plano, escrito todo en mayúsculas, sin espacios, tildes o puntos, es

<span style="font-family:Monospace;">NOSOLOCUENTASESMIBLOGFAVORITO</span>

Una manera de hacer que este mensaje sea ininteligible
es cambiando las letras de posición,
en lo que se conoce como un <b>cifrado por transposición</b>.
Los griegos tenían un procedimiento, llamado <b>escítala</b>,
por el que ponían esto en práctica.
Lo que hacían era enrollar en una vara la cinta en la que iban a escribir el mensaje,
y escribirlo de forma longitudinal en la vara, digamos, "de arriba a abajo";
el destinatario tenía una vara de igual grosor en torno a la cua
 enrollaba la cinta con el cifrado, y recuperaba así el mensaje original.
 En la práctica, no necesitamos varas para cifrar y descifrar el texto:
 el efecto de enrollar y escribir longitudinalmente
 es el de espaciar las letras en el mensaje cifrado.
 Conseguimos el mismo efecto si escribimos el mensaje de arriba a abajo, en una tabla,

<span style="font-family:Monospace;">NCSLO
OUEOR
SESGI
ONMFT
LTIAO
OABV</span>

y luego leemos en horizontal para obtener el mensaje cifrado,

<span style="font-family:Monospace;">NCSLOOUEORSESGIONMFTLTIAOOABV</span>

El problema de este cifrado es que no sería muy difícil de romper:
si sabes que se ha cifrado usando la escítala,
el truco es leer el mensaje saltando un número determinado de letras,
y mirar si el resultado tiene sentido.
Cierto es que nuestro mensaje de prueba es bastante corto,
y no hace falta probar muchas longitudes de salto hasta dar con la ganadora
(leer una letra, saltar cuatro, repetir),
pero no es tremendamente fuerte de todas formas. O, al menos, por sí solo.

Otro método de cifrado conocido en la Antigüedad es el <b>cifrado por sustitución</b>.
Aquí no cambias de orden las letras del mensaje,
sino que cambias cada letra individualmente de acuerdo a una regla dada.
Por ejemplo, puedes cambiar cada letra por la letra del abecedario
que está un determinado número de posiciones por delante.
Si, por ejemplo, decides sustituir cada letra por la que viene tres posiciones después,
puedes sustituir cada letra del alfabeto de arriba por la correspondiente del de abajo:

<span style="font-family:Monospace;">ABCDEFGHIJKLMNOPQRSTUVWXYZ
DEFGHIJKLMNOPQRSTUVWXYZABC</span>

y obtener el texto cifrado, a partir del texto plano original,

<span style="font-family:Monospace;">QRVRORFXHQWDVHVPLEORJIDYRULWR</span>

Y, puestos a sustituir cada letra del alfabeto original por una de un alfabeto nuevo,
podemos ser algo más creativos en nuestro diseño del alfabeto nuevo.
Una manera es escoger una palabra <b>clave</b> para colocar al comienzo del alfabeto.
Se entiende con un ejemplo: si usamos la palabra
<span style="font-family:Monospace;">CIFRADO</span>, obtenemos

<span style="font-family:Monospace;">CIFRADOBEGHJKLMNPQSTUVWXYZ</span>

El método introduce una cierta imprevisibilidad al método,
porque ya no sabes que todas las letras de tu mensaje
han sido desplazadas lo mismo dentro del abecedario.
Pero no está exento de riesgos,
pues elegir mal la clave puede hacer el mensaje bastante legible:
observa que usando la clave
<span style="font-family:Monospace;">CIFRADO</span>,
todas las letras a partir de la S se mantienen tal y como están;
la clave <span style="font-family:Monospace;">ABC</span> sería estúpida.
Además, en cualquier caso, el hecho de que toda letra del alfabeto original
se corresponda con una del nuevo es problemático.
Algo que se sabe es que, en un idioma dado,
no todas las letras aparecen con la misma frecuencia:
en castellano, no se ven tan a menudo las letras K o W como las letras E o A.
Si interceptas un mensaje cifrado del enemigo,
y observas que unas letras son más frecuentes que otras,
cabe suponer que eran originariamente E, A o alguna otra.
En mensajes cortos esto puede dar poca información,
ya que si el mensaje original era
<span style="font-family:Monospace;">KIWI</span>
el análisis de frecuencias no te llevará a buen puerto.
Pero si tienes mensajes largos, o muchos mensajes cortos cifrados de la misma forma,
sí puedes obtener conclusiones.
A fin de cuentas, se conoce bastante bien la distribución de letras en el castellano;
sin ir más lejos, la
<a href="https://es.wikipedia.org/wiki/Frecuencia_de_aparici%C3%B3n_de_letras">Wikipedia</a>
contiene hasta análisis de la frecuencia de aparición de letras
en El Quijote y La Regenta mostrando lo similares que son.
El análisis de frecuencias parece haber sido usado por primera vez
en el siglo IX por Al-Kindi,
así que estas técnicas no son precisamente nuevas,
pero tal vez no habías oído hablar de ellas.

Puedes fortalecer más tu cifrado si combinas transposición y sustitución,
ya que incluso si haces un análisis de frecuencias
no tienes por qué llegar al resultado correcto.
Pero otra opción es usar una transposición, digamos, variable.
Algo así como si para cada posible posición de una letra en tu mensaje
usaras un alfabeto distinto.
Un ejemplo de esto es el <b>cifrado de Vigenère</b>,
llamado así porque no fue inventado por Vigenère (o eso dicen).
Aquí fijamos una clave, por ejemplo <span style="font-family:Monospace;">HOLA</span>.
Como la clave tiene cuatro letras, usamos cuatro alfabetos distintos
y los reutilizamos cada cuatro letras de nuestro mensaje.
Los alfabetos que utilizaremos serán como el abecedario normal,
pero desplazado una serie de posiciones:
el asociado a la letra H de la clave es el alfabeto que empieza por H y termina en la G;
el asociado a la O, el que empieza por O y termina en la N; y así.
Puedes comprobar que cifrando nuestro mensaje en texto plano con esta clave
obtenemos el mensaje cifrado

<span style="font-family:Monospace;">UCDOSCNULBEAZSDMPPWONTLVVFTTV</span>

Este método parece lo suficientemente complicado
como para pensar que el cifrado será difícil de romper,
y sin embargo se puede ir toda la seguridad al garete si la clave que usamos es débil.
Lo complicado es hacerse una idea de cómo descifrar el mensaje cifrado sin conocer la clave,
pero el procedimiento en sí de cifrar y descifrar es relativamente sencillo
si la clave es conocida.
Así y todo, existen maneras de hacer un criptoanálisis a un cifrado de Vigenère:
si el mensaje es largo, se puede intentar un análisis de frecuencias por bloques
para averiguar la longitud de la clave.
No es trivial, pero es factible. Razón por la cual no se usa hoy en día.

Hay muchas maneras de rizar el rizo con estos métodos:
la máquina <b>Enigma</b>, usada por la Alemania nazi en la Segunda Guerra Mundial,
es uno de esos casos, en los que estas ideas se llevan al extremo
y complican enormemente la labor del criptoanálisis.
Pero tal vez necesitemos algo más de tiempo para hablar de estas cosas,
y creo que por hoy ya hemos hablado lo suficiente de estos cifrados clásicos.
Además, la idea es que durante los próximos días hablemos de otros cifrados en criptografía
que involucran algo más de matemáticas que los que hemos visto hoy.
Creo que conocer los casos clásicos ayuda a poner un poco en perspectiva los métodos más modernos,
pero posiblemente con lo de hoy ya estés suficientemente motivado.
Saldremos de dudas con el <em>one-time pad</em>.

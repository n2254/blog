---
title: "Un grupo, un anillo y un cuerpo entran en un blog - NSC #10"
categories: ["matemáticas"]
tags: ["anillos", "aritmética modular", "álgebra", "cuerpos", "fundamentos", "grupos", "jerga"]
slug: un-grupo-un-anillo-y-un-cuerpo-entran-en-un-blog-nsc-10
aliases: [/archivo/2018/08/25/un-grupo-un-anillo-y-un-cuerpo-entran-en-un-blog-nsc-10/]
date: 2018-08-25T13:54:30+0000
lastmod: 2018-08-25T13:54:30+0000
---

Es difícil ponerse a hablar de álgebra y conceptos relacionados
sin manejar un poco de jerga.
Recuerdo que esto fue lo que estudié
en las primeras clases de Álgebra Lineal en la carrera,
y realmente son términos que aparecen una y otra vez
y que complican la vida del algebrista que reniega de ellos.
Una cosa es que queramos mantener las matemáticas comprensibles,
y otra convertirlas en largas listas de propiedades.
Las cosas tienen nombre, ¿sabes? Así que empecemos.

Los números, por sí solos, dan para poco. Frase atrevida, pero escúchame.
Sabemos que con ellos, al menos con los naturales,
se puede ordenar y contar cosas.
Eso es bueno, pero no suficiente.
También queremos sumarlos, restarlos, multiplicarlos, y demás.
Por eso, tan importante como "la cosa en sí" que son los números individualmente,
son las operaciones definidas en el conjunto de esos números
lo que los hace interesantes.
Al menos, en el álgebra. En álgebra no estudias simplemente elementos
–por ejemplo, números– de forma aislada del resto.
Primero consideras un <b>conjunto</b> de dichos elementos,
para poder estudiarlos todos a la vez –<em>conjuntamente</em>, si me lo permites–.
Y, una vez tienes el conjunto, defines una operación,
o más largamente <b>operación binaria interna</b>,
es decir, una función que a cada par de elementos del conjunto
les asigne otro del mismo conjunto.

¿Demasiada jerga demasiado pronto?
Piensa en la suma en los números, o en el producto.
¿Acaso no es la suma una función que, a cada par de números \\(a\\) y \\(b\\),
les asigna el número \\(a+b\\)?
¿O el producto una función que asigna al par de números \\(a\\) y \\(b\\)
el número \\(a·b\\)?
Tanto la suma como el producto son operaciones binarias internas,
definidas sobre el conjunto correspondiente,
sea éste el de los números naturales \\(\mathbf N\\),
el de los enteros \\(\mathbf Z\\),
el de los racionales \\(\mathbf Q\\)
o el de los reales \\(\mathbf R\\).
No estamos más que asignando nombres a conceptos que conocíamos.

Sigamos. Supongamos que tenemos una operación binaria interna así.
Ya que tenemos estos ejemplos de la suma y el producto,
podemos denotar nuestra operación de forma que sugiera
una conexión con las operaciones estándar.
Por ejemplo, denotando el resultado de la operación sobre \\(a\\) y \\(b\\)
como \\(a\ast b\\). Bien.
Resulta que la suma y el producto de números tiene propiedades interesantes
que probablemente hayas escuchado a menudo de pequeño.
Algo como que la suma es conmutativa y asociativa, por ejemplo.
Decir que la suma es conmutativa es decir
que el orden de los sumandos no altera el total.
Que la suma es asociativa quiere decir que,
dados tres números \\(a\\), \\(b\\) y \\(c\\),
la igualdad siguiente es cierta: \\((a+b)+c= a+(b+c)\\).
Podemos adaptar este concepto a nuestra operación binaria interna,
más general, a golpe de definición.
Decimos que una operación binaria interna \\(\ast \\)
– esto es muy largo, voy a empezar a llamarla OBI, ¿vale? –
definida sobre un conjunto \\(C\\) (la "C" es de conjunto)
es <b>asociativa</b> si,
dados \\(a\\), \\(b\\) y \\(c\\) en \\(C\\),
se tiene que
\\[(a\ast b)\ast c = a\ast (b\ast c),\\]
y decimos que \\(\ast \\) es <b>conmutativa</b> si,
dados \\(a\\) y \\(b\\) en \\(C\\),
\\[a\ast b = b\ast a.\\]

Nada nuevo bajo el sol,
más allá del hecho de hablar ahora de OBI sobre conjuntos arbitrarios
en lugar de hablar de sumas y productos de números.
Sigamos. Otra noción de interés es
la existencia de elementos especiales para esta operación.
Para la suma, tanto \\(\mathbf Z\\), \\(\mathbf Q\\) y \\(\mathbf R\\)
tienen lo que se llama un elemento neutro, es decir,
un número que sumado a cualquier otro lo deja como está;
en estos casos, ese elemento neutro sería el número 0.
Asimismo, también existe la noción de elemento opuesto o inverso a uno dado:
si tienes un número \\(a\\), existe un número,
denotado \\(-a\\), tal que \\(a+(-a)= 0\\).
De nuevo, podemos generalizar esto a nuestra OBI.
Dada una OBI \\(\ast \\) definida sobre un conjunto \\(C\\),
un elemento \\(e\\) de \\(C\\) se denomina
<b>elemento neutro de </b>\\(\ast \\)
si, para cualquier elemento \\(a\\) de \\(C\\), se tiene
\\[
\begin{align*}
a\ast e = a
&&\text{y}
&&e\ast a = a.
\end{align*}
\\]
Y, dado un elemento \\(a\\) de \\(C\\),
un elemento \\(b\\) de \\(C\\) se llamará
<b>elemento inverso de \\(a\\)</b> si
\\[
\begin{align*}
a\ast b = e
&&\text y
&&b\ast a = e.
\end{align*}
\\]

Éstas son las propiedades típicas que uno suele mencionar
cuando habla de la suma y el producto de números:
son operaciones conmutativas, asociativas,
con elemento neutro y con elementos inversos.
Esta cuaterna de propiedades se dan bastante a menudo en álgebra,
así que se suelen resumir con otras palabras clave.
Por tanto, se dice que un conjunto \\(C\\) y una OBI \\(\ast \\)
definida sobre \\(C\\) forman un <b>grupo</b>
si \\(\ast \\) es asociativa, tiene elemento neutro,
y tiene elementos inversos.
¿Por qué no exigirle también que sea conmutativa?
Vale, he mentido: el caso no conmutativo también ocurre a menudo,
y merece la pena distinguirlo del resto.
Cuando la OBI es también conmutativa el grupo se denomina <b>abeliano</b>,
en honor a un matemático noruego llamado Abel.

¿Ejemplos de grupos? Ya hemos hablado de algunos:
tanto en \\(\mathbf Z\\), \\(\mathbf Q\\) y en \\(\mathbf R\\)
la suma tiene las propiedades asociativa, conmutativa,
tenencia de elemento neutro y de inversos,
por lo que \\((\mathbf Z,+)\\), \\((\mathbf Q,+)\\) y \\((\mathbf R,+)\\)
son, los tres, grupos abelianos.
De hecho, en la gran mayoría de las veces que uno tiene un grupo abeliano,
a la OBI se le suele denominar <b>suma</b>,
y suele ser representada con el símbolo \\(+\\);
también su elemento neutro se denota por 0,
y el inverso de \\(a\\) se suele llamar <b>opuesto</b>
y denotar por \\(-a\\).
Como hay casos en los que la OBI no es conmutativa,
y sólo da lugar a un grupo –sin el apellido de abeliano, entiéndase–,
se reserva la notación aditiva para los casos conmutativos
y se usa otra algo más genérica para los que no son conmutativos.

Hay más ejemplos de grupos abelianos.
Por ejemplo, uno estúpido: el formado únicamente por el elemento neutro 0.
Como 0+0=0, la operación es interna,
y es obviamente asociativa, conmutativa,
con neutro 0 y donde el único inverso es el de 0, que es igual a 0.
Este grupo se denomina <b>grupo trivial</b>, por razones obvias.
Otros ejemplos son los anillos de restos módulo \\(N\\),
<a href="https://nosolocuentas.wordpress.com/2018/06/22/cuenta-hasta-cero-nsc-8/">de los que ya hablamos hace tiempo</a>:
en todos ellos, la suma da lugar a un grupo abeliano.
En cuanto a un ejemplo de cosa que no es grupo (ni abeliano ni leches),
tenemos los números naturales \\(\mathbf N\\).
Se puede sumar, y la suma es conmutativa y asociativa.
Hay cero, al menos según la definición que uses
–<a href="https://nosolocuentas.wordpress.com/2018/02/18/aprendiendo-los-numeros-nsc-1/">y la nuestra lo usa</a>–.
Pero no hay opuestos: la gracia de los enteros
es que están ahí para completar esta estructura
de los naturales y hacerla un grupo.
(Por cierto, la estructura de los naturales también tiene nombre,
el de <em>semigrupo</em>, o incluso <em>monoide</em>,
pero eso lo ignoraremos por ahora.)

En realidad, todos los ejemplos que acabo de mencionar
son también ejemplos de un caso particular de grupo abeliano,
llamado <em>anillo</em>.
Supón que tienes un grupo abeliano \\((G,+)\\), es decir,
un conjunto \\(G\\) que es grupo (de ahí que lo llame "G")
junto con la OBI \\(+\\).
Igual que conseguiste la OBI \\(+\\), podrías tener otra OBI extra,
llamémosla \\(\ast \\).
Esta nueva OBI podría representar el producto, por ejemplo,
cuando tu conjunto \\(G\\) es igual a \\(\mathbf Z\\) y ya tienes la suma.
En los números, cuando consideras simultáneamente el producto y la suma,
resulta que se portan bien la una con la otra, en el siguiente sentido:
si tienes tres números \\(a\\), \\(b\\) y \\(c\\),
se tiene siempre que \\(a·(b+c)= a·b+a·c\\) y \\((a+b)·c= a·c+b·c\\).
Pues bien, tu nueva OBI \\(\ast \\) se llamará
<b>distributiva respecto de </b>+ si se tienen las igualdades análogas,
es decir, para cualesquiera \\(a\\), \\(b\\) y \\(c\\) en \\(G\\),
se tiene que
\\[
\begin{align*}
a\ast (b+c) = a\ast b+a\ast c
&&\text y
&&(a+b)\ast c = a\ast c+b\ast c.
\end{align*}
\\]

Además de distributiva, tu OBI puede ser también asociativa,
o conmutativa, o tener elemento neutro, o tener inversos.
Si tiene elemento neutro, casi siempre se le llamará <em>1</em>,
para acentuar esta semejanza con el producto de números,
y al inverso de \\(a\\) se le denotará a menudo como \\(a^{-1}\\).
El caso es que aquí se introducen nuevas notaciones.
Si tu conjunto \\(G\\) viene equipado con una OBI \\(+\\)
que lo convierte en grupo abeliano,
y con una OBI extra \\(\ast \\)
que es asociativa y distributiva con respecto de \\(+\\),
se dice que \\(G\\) con \\(+\\) y \\(\ast \\) forman un <b>anillo</b>,
e igual que a la primera OBI \\(+\\) se le suele llamar suma,
a la segunda OBI se le suele llamar <b>producto</b>.
Como tener un producto \\(\ast \\)
únicamente asociativo y distributivo con respecto de \\(+\\) es un poco cutre,
a veces se le suele requerir un poco más,
como que sea conmutativo y que tenga elemento neutro 1.
(Así y todo, esto le parece aburrido a muchos matemáticos,
que sienten el placer del desafío cuando se enfrentan a productos
que no son ni asociativos. Así somos.)

Bien, resulta que todos los ejemplos de grupos de los que he hablado
son anillos, incluso conmutativos y con identidad.
En el caso del <b>anillo trivial</b>,
el producto es igual que la suma: \\(0\ast 0= 0\\),
por lo que el elemento neutro 1 es igual a 0.
Es tremendamente estúpido, pero es así:
en el anillo trivial, 1=0.
Los demás son un poco más normales.
Aunque, si te fijas, verás diferencias entre ellos.
Por ejemplo, en \\((\mathbf Z,+,\cdot)\\),
no todos los elementos tienen inverso:
no existe ningún número entero que multiplicado por 2 dé 1.
En \\((\mathbf Q,+,\cdot)\\), todos los elementos tienen inverso, salvo el 0;
lo mismo ocurre cuando consideramos los números reales.
En el caso de los anillos de restos módulo \\(N\\),
hemos visto antes que tienen propiedades diversas.
Por ejemplo, \\(\mathbf Z/5\mathbf Z\\)
tiene inversos para todos sus elementos salvo el cero;
sin embargo, \\(\mathbf Z/4\mathbf Z\\)
no sólo no tiene inversos para todos sus elementos no nulos,
sino que incluso tiene elementos distintos de cero
cuyo producto es cero: en este anillo, \\(2·2= 0\\).
Por cierto, que existe un nombre ingenioso para estos elementos:
se les llama <b>divisores de cero</b>.

¿Hay algún anillo donde el cero tenga <em>inverso multiplicativo</em>,
es decir, inverso con respecto a la segunda OBI que da lugar al anillo?
Sí, el anillo trivial. ¿Alguno más? Va a ser complicado.
Para eso necesitamos un elemento \\(a\\) que, multiplicado por 0, nos dé 1.
Esto parece absurdo con los números,
porque en ellos multiplicar por cero siempre da cero.
Resulta que, gracias a haber definido los anillos a semejanza de los números,
esto también ocurre con cualquier anillo. Y lo podemos demostrar.

<hr>

<b>PROPOSICIÓN.</b> Sea \\((A,+,\ast )\\) un anillo.
Entonces, para todo \\(a\\) en \\(A\\),
se tiene que \\(a\ast 0=0\\) y \\(0\ast a= 0\\).

<b>DEMOSTRACIÓN.</b> El cero es el elemento neutro de la suma,
así que sumado a cualquier elemento lo deja como está;
en particular, si lo sumas a sí mismo, obtienes
\\[0+0 = 0.\\]
Podemos multiplicar ahora esto por \\(a\\), a ambos lados:
\\[a\ast (0+0) = a\ast 0.\\]
Como el producto es distributivo con respecto de la suma, obtenemos
\\[a\ast 0+a\ast 0 = a\ast 0.\\]
No sabemos (aún) lo que es \\(a\ast 0\\),
pero sí sabemos que tiene que tener un elemento opuesto: \\(-a\ast 0\\).
Sumándolo a ambos lados, obtenemos
\\[(a\ast 0+a\ast 0)+(-a\ast 0) = a\ast 0+(-a\ast 0).\\]
Como partimos de un anillo, en particular tenemos un grupo abeliano,
luego la suma es asociativa: usando esta propiedad en el lado izquierdo, vemos que
\\[a\ast 0+(a\ast 0+(-a\ast 0)) = a\ast 0+(-a\ast 0).\\]
Pero, por definición de opuesto, \\(a\ast 0+(-a\ast 0)= 0\\), por lo que
\\[a\ast 0+0 = 0.\\]
Y usando ahora que 0 es el elemento neutro de la suma, vemos que
\\[a\ast 0 = 0.\\]
Q.E.D.

<hr>

Moraleja: si quieres dividir por cero, no llames a un algebrista.

Esto muestra que un anillo donde el cero tenga inverso va a tener que ser trivial.
Y ese anillo no es demasiado interesante de estudiar:
no es que pase gran cosa dentro de él.
Pero podemos pedirle al anillo que contenga inversos para todos los demás elementos,
lo que parece bastante razonable
(a ver, los enteros no tienen inversos para todos los elementos,
pero algunos anillos chulos como los racionales o los reales sí).
Podemos sumar esta exigencia a otras exigencias razonables,
como la de que en el anillo no haya divisores de cero,
o que el producto sea conmutativo.
Y todas estas condiciones juntas merecen una palabra en jerga que las resuma.
Si tu anillo \\((A,+,\ast )\\) (la "A" por... ya sabes...)
es tal que la OBI \\(\ast \\) tiene elemento neutro 1,
es conmutativa, no da lugar a divisores de cero,
y todo elemento no nulo tiene inverso,
el anillo se denomina <b>cuerpo</b>.

¿Ejemplos de cuerpos? Precisamente \\(\mathbf Q\\) o \\(\mathbf R\\), por ejemplo.
También los complejos, \\(\mathbf C\\).
Y también los de la forma \\(\mathbf Z/p\mathbf Z\\) cuando \\(p\\) es primo.
¿Alguno más? Sí, pero ésos los veremos más adelante.
Aunque dejé una pista cuando hablé de dimensiones...

Suficiente por hoy. Descansa (no es una orden, ¿eh?).

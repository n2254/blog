---
title: "El origen - NSC #0"
categories: ["meta"]
slug: primera-entrada-del-blog
aliases: [/archivo/2018/01/31/primera-entrada-del-blog/]
date: 2018-01-31T12:55:05+0000
lastmod: 2018-02-01T11:30:22+0000
---

Llevaba tiempo queriendo escribir un blog. Más concretamente, uno acerca de matemáticas. No me faltan ejemplos de gente que cree que, por la carrera que he estudiado, debería ser capaz de hacer complejas cuentas sin esfuerzo, repartir la cuenta proporcionadamente tras una cena, o arreglar cualquier ordenador que se me ponga por delante. Si te sientes reconocido en esta descripción, espero que entiendas que no lo digo con acritud: no creo que lo hagas con mala intención, pues a fin de cuentas nos halagas a los matemáticos al creernos poseedores de semejantes talentos. Pero sí creo que es un reflejo de la educación matemática que recibimos.

Piénsalo. Nos pasamos, o al menos yo me pasé, las matemáticas de Primaria (sustituye aquí el nombre apropiado para esa etapa escolar en tu vida) haciendo sumas, restas, multiplicaciones, divisiones, potencias, y haciendo alguna cosilla básica de geometría. Cuentas largas, eso sí: saber dividir un número de siete dígitos por uno de cuatro no significa que puedas hacer lo mismo con uno de ocho dígitos. Pasar a la ESO es hacer esas cuentas largas, pero con letras de por medio, lo que las convierte en *ecuaciones*. La geometría se convierte en calcular perímetros, áreas y volúmenes, y aparecen unos objetos llamados *funciones*, a los que debes suponer algún tipo de interés más allá del "se parecen a ecuaciones". Si seguiste estudiando matemáticas, puede que aparecieran cosas de estadística, tal vez algo más de geometría, y luego cosas de nombres tan elegantes como *números complejos*, *vectores*, *matrices*, *derivadas* o *integrales*.

Todo cuentas. Todo. Lo que, si tienes en cuenta (ejem) que son tareas que un ordenador o calculadora puede hacer sin esfuerzo, suena a pérdida de tiempo. Ojo: no lo es, pues te puede ayudar a pensar en números desde una perspectiva más crítica, evitando el anumerismo. Pero es casi inevitable acabar con una visión bastante equivocada de lo que son las Matemáticas, esa disciplina que, de una forma o de otra, ha sido estudiada por numerosas civilizaciones a lo largo de la Historia (a bote pronto: babilonios, egipcios, griegos, chinos, indios, árabes y occidentales). De ahí que lleve tiempo queriendo escribir algo acerca de esa parte de las Matemáticas que no se enseña en los colegios,
y de la que apenas se habla en la vida cotidiana. Matemáticas tal vez poco útiles para el día a día, lo reconozco. Al menos, tan poco útiles como la filosofía, la pintura, la música y la literatura.

Y, ¿quién quiere vivir en un mundo donde lo único que merece la pena conocer es aquello que te permita ejercer mecánicamente un oficio?

En las próximas entradas, hablaré de temas en matemáticas que me parezcan interesantes. La mayoría, al menos al principio, no tratarán tanto lo último en investigación en matemáticas como conceptos algo más cercanos (espero).

La próxima vez, hablaré de números. Los que conoces desde pequeño. Más o menos.

<!-- 
  <category domain="category" nicename="meta"><![CDATA[Meta]]></category>
-->

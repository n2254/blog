<item>
  <title>Agrupando permutaciones - NSC #11</title>
  <link>https://nosolocuentas.wordpress.com/2018/09/06/agrupando-permutaciones-nsc-11/</link>
  <pubDate>Thu, 06 Sep 2018 09:11:16 +0000</pubDate>
  <dc:creator>guicafer</dc:creator>
  <guid isPermaLink="false">https://nosolocuentas.wordpress.com/?p=61</guid>
  <description/>
  <content:encoded><![CDATA[Después de tanto hablar de jerga, cambiemos algo de tercio. La situación que te traigo es la siguiente. Tienes tres bolas distintas, a las que vamos a llamar A, B y C. ¿Cuántas maneras distintas hay de reordenarlas?

<img class="alignnone size-full wp-image-66" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-0.png" alt="Tres bolas con una letra cada una, ordenadas como A B C." width="946" height="158" />

Un razonamiento es como sigue. La bola A puede ir en tres posiciones distintas: puede ser la primera, la del medio, o la última. Sin saber en qué posición va la bola A, lo único que sabemos es que la bola B no puede ocupar la posición que ocupa la A, y por tanto sólo tiene dos posiciones a su, digamos, disposición. Por ejemplo, si la bola A está en el medio, la bola B sólo puede ir en una de estas posiciones: primera, o última. Finalmente, una vez las posiciones de las bolas A y B están concretadas, la bola C sólo tendrá una posibilidad: ocupar la posición no ocupada por las otras dos bolas. En resumen: hay 3 posibilidades para A, por cada una de ellas hay 2 posibilidades para B, y por cada una de ellas hay 1 posibilidad para C. Es decir, el número de posiciones es 3·2·1 = 6. Bien. Ahora, por lo pronto, parece que este razonamiento no depende en esencia de que hayamos empezado con tres bolas. Y, de hecho, se puede extender a cualquier número de bolas: si quisieras reordenar 5 bolas, tendrías 5·4·3·2·1 = 125 posibilidades. Esta manera de construir números, a partir de multiplicar todos los números entre 1 y uno dado, es lo que se llama factorial: el <b>factorial</b> de un número <em>N</em>, denotado <em>N</em>!, es el número
<p style="text-align:center;"><em>N</em>! = <em>N</em>·(<em>N</em>-1)·s 2·1.</p>
Bien. Volvamos a nuestras bolas de partida.

<img class="alignnone size-full wp-image-66" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-0.png" alt="Tres bolas con una letra cada una, ordenadas como A B C." width="946" height="158" />

Supongamos que las has reordenado de una de las 3! = 6 maneras disponibles; por ejemplo, pueden haber acabado así:

<img class="alignnone size-full wp-image-67" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-1.png" alt="Las tres bolas han cambiado de orden, ahora ordenadas como B C A." width="946" height="395" />

Imagínate ahora que quieres reordenarlas una segunda vez. Bien, igual que antes, tienes 6 posibilidades de reordenarlas. Hasta aquí, todo en orden. Pero quiero llamar tu atención sobre un hecho, rematadamente obvio una vez lo haya dicho, pero en el que tal vez no hayas pensado conscientemente: si reordenas las bolas en la configuración actual y obtienes una nueva, tercera, configuración, será como si hubieras reordenado directamente la configuración original hasta obtener la tercera. Por ejemplo, supón que tu tercera configuración fuera

<img class="alignnone size-full wp-image-65" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-2.png" alt="Las tres bolas han vuelto a cambiar de orden, ahora ordenadas como C B A." width="946" height="355" />

El proceso ha sido
<p style="margin-left:2em;margin-right:2em;"><em>reordenar la configuración inicial para que la primera bola pase a ser la tercera, la segunda bola pase a ser la primera, y la tercera bola pase a ser la segunda</em></p>
seguido de
<p style="margin-left:2em;margin-right:2em;"><em>reordenar la configuración intermedia para que la primera bola pase a ser la segunda, y la segunda bola pase a ser la primera.</em></p>
Pero esto es equivalente a
<p style="margin-left:2em;margin-right:2em;"><em>reordenar la configuración inicial para que la primera bola pase a ser la tercera, y la tercera bola pase a ser la primera</em>.</p>

En forma más sucinta, como si fuera un eslógan:
<p style="margin-left:2em;margin-right:2em;"><em>un reordenamiento de un reordenamiento es un reordenamiento</em>.</p>

Te dije que iba a ser algo obvio. Lo que igual no es tan obvio es lo que voy a plantearte ahora: olvídate de las bolas, y céntrate ahora en los <em>reordenamientos</em>. Si te extraña, recuerda que esto son matemáticas, y que no nos asusta olvidar la realidad para pensar en abstracciones: en este caso, pensar en los reordenamientos como si fueran cosas en lugar de centrarnos en las cosas que ordenan. No es algo tan distinto a lo que hacíamos con las funciones <a href="https://nosolocuentas.wordpress.com/2018/03/19/aprendiendo-a-contar-nsc-4/">hace un tiempo</a>, y de hecho las funciones que considerábamos eran precisamente reordenamientos. Pero igual entonces no hicimos tanto énfasis en cómo centrar nuestra atención en las funciones y no tanto en las cosas sobre las que actúan puede ser de interés. Esta idea es esencial, y aparece con gran frecuencia en matemáticas: ¿quieres entender un objeto?, pues estudia las funciones que actúan sobre él.

Ya que vamos a ver nuestros reordenamientos como funciones, será mejor terminar de traducir a la jerga funcional. Aplicar una función, llamémosla <em>f</em> a un objeto, y luego aplicar una nueva función, sea <em>g</em>, al resultado, se denomina <b>componer las funciones </b><em>f</em><b> y </b><em>g</em>. La <b>composición de </b><em>f</em><b> y </b><em>g</em> es la función resultante, denotada como <em>g∘f</em>: aquella función que, aplicada sobre un objeto, tiene el mismo efecto que aplicar primero <em>f</em> y luego <em>g</em>. Si el objeto es representado por <em>x</em>, lo que esto dice es que
<p style="text-align:center;">(<em>g∘f</em>)(<em>x</em>) := <em>g</em>(<em>f</em>(<em>x</em>)).</p>
Esto es la versión funcional de encadenar reordenamientos: encadenar funciones. Encadenar dos funciones da lugar a otra función, y encadenar dos reordenamientos da lugar a otro reordenamiento. De ahí que queramos ver unos como las otras. Más propiedades. En un reordenamiento, las bolas siguen siendo las mismas, sólo cambia su orden. No hay dos bolas que se fusionen y ocupen la misma posición, y toda bola en la nueva configuración estaba en la original. Esto se corresponde con una función <em>biyectiva</em>: nunca repite los valores que toma, y toma todos los posibles. Realmente, esta definición de función biyectiva es tremendamente similar a la del reordenamiento, y lo que acaba ocurriendo es que <em>las funciones biyectivas que toman la posición actual de una bola y le asignan una nueva posición, dentro de un conjunto finito de bolas, coinciden con los reordenamientos de dichas bolas</em>.

¿Sigues aquí? ¿Te ha espantado toda esta charla seria y abstracta sobre cómo ordenar bolas? Comprendería que te haya entrado el sueño, de hecho. El asunto es que esto sólo acaba de empezar. Hay una propiedad muy, muy interesante acerca de estas funciones reordenamiento, y tiene que ver con su composición. Comprenderás que no estuve introduciendo jerga <a href="https://nosolocuentas.wordpress.com/2018/08/25/un-grupo-un-anillo-y-un-cuerpo-entran-en-un-blog-nsc-10/">el otro día</a> para nada, ¿verdad? Si la composición de dos funciones es otra función, esto puede dar lugar a una operación binaria interna. En el caso de las funciones reordenamiento, lo es, porque su composición no es una función cualquiera, sino otro reordenamiento. ¿Qué propiedades tendría esta operación? Bueno, técnicamente dejar las bolas en la posición que estaban es un reordenamiento, uno trivial que no hace nada, así que reordenar antes o después de "dejarlas como están" es lo mismo que reordenar sin más. Este reordenamiento trivial funcionaría, así, como elemento neutro. Luego, si reordenas las bolas de una manera, deshacer ese reordenamiento es también reordenarlas, y el resultado de reordenar y deshacerlo es dejarlas como estaban, es decir, el reordenamiento trivial. Así que el inverso de un reordenamiento sería aquél que lo deshace. ¿Y la asociatividad? La asociatividad se tiene siempre para funciones, de hecho: se pueden componer funciones agrupándolas como se quiera. El caso de los reordenamientos es un caso particular de esto, así que la composición de reordenamientos es asociativa.

Bien, pues. El conjunto de las funciones reordenamiento, equipado junto a la operación composición, da lugar a un grupo. ¿Es abeliano, es decir, conmutativo? Eso es fácil de ver. Hemos hecho ya dos reordenamientos de tres bolas: primero pasar la primera bola a la última posición, desplazando las otras dos hacia delante, y luego cambiando de orden las dos primeras bolas:

<img class="alignnone size-full wp-image-66" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-0.png" alt="Tres bolas con una letra cada una, ordenadas como A B C." width="946" height="158" />
<img class="alignnone size-full wp-image-67" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-1.png" alt="Las tres bolas han cambiado de orden, ahora ordenadas como B C A." width="946" height="395" />
<img class="alignnone size-full wp-image-65" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-2.png" alt="as tres bolas han vuelto a cambiar de orden, ahora ordenadas como C B A." width="946" height="355" />

¿Qué hubiera ocurrido si los hubiéramos hecho al revés? Primero intercambiar las dos primeras bolas,

<img class="alignnone size-full wp-image-66" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-0.png" alt="Tres bolas con una letra cada una, ordenadas como A B C." width="946" height="158" /> <img class="alignnone size-full wp-image-64" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-3.png" alt="Las tres bolas han cambiado de orden, ahora ordenadas como B A C." width="946" height="355" />

y luego pasar la primera bola al final,

<img class="alignnone size-full wp-image-63" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-4.png" alt="Tres bolas con una letra cada una, ordenadas como B A C." width="946" height="158" /> <img class="alignnone size-full wp-image-62" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc11fig-5.png" alt="Las tres bolas han cambiado de orden, ahora ordenadas como A C B." width="946" height="395" />

El resultado es visiblemente distinto: componer reordenamientos no es conmutativo. ¿Es esto un problema? No, para nada. Que los ejemplos usuales de grupos en los que uno puede pensar la primera vez sean abelianos no significa nada: la suma de números tendrá una propiedad más que la composición de reordenamientos, pero no puede describir esta situación. Los reordenamientos son esencialmente no conmutativos, y necesitamos una estructura matemática no conmutativa que los describa.

El caso es que este grupo tiene un nombre especial. Se le denota como <em>S<sub>3</sub></em>, y se conoce como el <b>grupo de permutaciones de 3 elementos</b>, o también <b>grupo simétrico de 3 elementos</b>. Los elementos del grupo, esto es, las funciones reordenamiento, se suelen denominar <b>permutaciones</b>. El grupo <em>S<sub>3</sub></em> es un caso particular, claro, de grupo de permutaciones: si en lugar de haber tenido 3 bolas hubiéramos tenido <em>n</em>, habríamos obtenido el grupo de permutaciones de <em>n</em> elementos, denotado <em>S<sub>n</sub></em>. Las permutaciones también tienen una manera abreviada de describirse, por cierto, en forma de <b>ciclo</b>: si intentas decir

<p style="margin-left:2em;margin-right:2em;"><em>la bola en posición 1 pasa a la posición 2, la bola en posición 2 pasa a la posición 3, y la bola en posición 3 pasa a la posición 1</em>,</p>

puedes escribir simplemente
<p style="text-align:center;">(1 2 3)</p>
y en lugar de

<p style="margin-left:2em;margin-right:2em;"><em>la bola en posición 1 pasa a la posición 2, y la bola en posición 2 pasa a la posición 1</em>,</p>

escribir
<p style="text-align:center;">(1 2)</p>
Según esta notación, si tienes una bola en la posición <em>n</em> y quieres saber dónde acaba tras la permutación, tendrías que buscar el número <em>n</em> en el ciclo: si no aparece, es que la bola no cambia de posición, y si aparece, pasa a la posición con el número siguiente en el ciclo; si el número <em>n</em> estaba al final del ciclo, pasa a la posición dada por el primer número del ciclo, de ahí el nombre. La permutación trivial, al ser elemento neutro en un grupo no conmutativo, se suele representar como 1, sin más, y el "producto" de permutaciones, es decir, su composición, se escribiría juntando los ciclos —<em>yuxtaponiéndolos</em>, digamos—:
<p style="text-align:center;">(1 2)(1 2 3) = (2 3)</p>
<p style="text-align:center;">(1 2 3)(1 2) = (1 3)</p>
Las reglas de cómo se calculan los productos de ciclos no son demasiado difíciles de ver, pero tampoco necesito hablar de ellas ahora. Si quieres deducirlas tú, te recordaré que las permutaciones en esencia son funciones, y que cuando uno compone funciones, la que se escribe a la izquierda es la que actúa <em>después</em>, no antes. Es decir, el producto (1 2)(1 2 3) quiere decir que primero se reordenan las bolas según el ciclo (1 2 3), y luego el resultado se reordena según (1 2).

Por ahora nada más que decir, salvo llamarte la atención respecto de un detalle. El grupo <em>S<sub>n</sub></em> se denomina también <em>grupo simétrico</em>, y no por casualidad. Si piensas en las transformaciones de simetría usuales, rotaciones y reflexiones de un cuadrado por ejemplo, podrás ver similitudes con respecto a los reordenamientos: componer dos transforaciones de simetría da lugar a otra transformación de simetría, y toda transformación de simetría se puede deshacer. De hecho, podrías ir más allá, y pensar que tal vez un reordenamiento de un conjunto de bolas es una especie de transformación de simetría del conjunto: la respuesta a qué tipo de transformaciones del conjunto ordenado de <em>n</em> bolas hacen que siga siendo un conjunto ordenado de las mismas <em>n</em> bolas. Y quien dice bolas, dice elementos, puntos... o vértices de una figura geométrica. Eso, para la próxima.]]></content:encoded>
  <excerpt:encoded><![CDATA[]]></excerpt:encoded>
  <wp:post_id>61</wp:post_id>
  <wp:post_date>2018-09-06 09:11:16</wp:post_date>
  <wp:post_date_gmt>2018-09-06 09:11:16</wp:post_date_gmt>
  <wp:post_modified>2018-09-08 13:10:28</wp:post_modified>
  <wp:post_modified_gmt>2018-09-08 13:10:28</wp:post_modified_gmt>
  <wp:comment_status>open</wp:comment_status>
  <wp:ping_status>open</wp:ping_status>
  <wp:post_name>agrupando-permutaciones-nsc-11</wp:post_name>
  <wp:status>publish</wp:status>
  <wp:post_parent>0</wp:post_parent>
  <wp:menu_order>0</wp:menu_order>
  <wp:post_type>post</wp:post_type>
  <wp:post_password/>
  <wp:is_sticky>0</wp:is_sticky>
  <category domain="category" nicename="algebra"><![CDATA[Álgebra]]></category>
  <category domain="category" nicename="fundamentos"><![CDATA[Fundamentos]]></category>
  <category domain="post_tag" nicename="grupo-simetrico"><![CDATA[Grupo simétrico]]></category>
  <category domain="post_tag" nicename="grupos"><![CDATA[Grupos]]></category>
  <wp:postmeta>
    <wp:meta_key>timeline_notification</wp:meta_key>
    <wp:meta_value><![CDATA[1536225078]]></wp:meta_value>
  </wp:postmeta>
  <wp:postmeta>
    <wp:meta_key>_rest_api_published</wp:meta_key>
    <wp:meta_value><![CDATA[1]]></wp:meta_value>
  </wp:postmeta>
  <wp:postmeta>
    <wp:meta_key>_rest_api_client_id</wp:meta_key>
    <wp:meta_value><![CDATA[-1]]></wp:meta_value>
  </wp:postmeta>
  <wp:postmeta>
    <wp:meta_key>_publicize_job_id</wp:meta_key>
    <wp:meta_value><![CDATA[21851395415]]></wp:meta_value>
  </wp:postmeta>
  <wp:comment>
    <wp:comment_id>10</wp:comment_id>
    <wp:comment_author><![CDATA[Agrupando simetrías &#8211; NSC #12 &#8211; No sólo cuentas]]></wp:comment_author>
    <wp:comment_author_email/>
    <wp:comment_author_url>https://nosolocuentas.wordpress.com/2018/09/20/agrupando-simetrias-nsc-12/</wp:comment_author_url>
    <wp:comment_author_IP>192.0.99.140</wp:comment_author_IP>
    <wp:comment_date>2018-09-20 15:20:43</wp:comment_date>
    <wp:comment_date_gmt>2018-09-20 15:20:43</wp:comment_date_gmt>
    <wp:comment_content><![CDATA[[&#8230;] La última vez vimos que los reordenamientos de n bolas, lo que podríamos considerar sus simetrías, forman un grupo, el grupo de permutaciones de n elementos, denotado Sn. Tal vez te preguntes qué tiene que ver esto con simetrías, digamos, usuales: por ejemplo, las de figuras geométricas sencillas. ¿Qué dice esto acerca de las simetrías de un triángulo equilátero, por ejemplo? Bien, si damos nombre a los vértices del triángulo, recuperamos una configuración de tres bolas, casi como la de la última vez: [&#8230;]]]></wp:comment_content>
    <wp:comment_approved>0</wp:comment_approved>
    <wp:comment_type>pingback</wp:comment_type>
    <wp:comment_parent>0</wp:comment_parent>
    <wp:comment_user_id>0</wp:comment_user_id>
    <wp:commentmeta>
      <wp:meta_key>akismet_result</wp:meta_key>
      <wp:meta_value><![CDATA[false]]></wp:meta_value>
    </wp:commentmeta>
    <wp:commentmeta>
      <wp:meta_key>akismet_history</wp:meta_key>
      <wp:meta_value><![CDATA[a:2:{s:4:"time";d:1537456844.356333;s:5:"event";s:9:"check-ham";}]]></wp:meta_value>
    </wp:commentmeta>
    <wp:commentmeta>
      <wp:meta_key>akismet_history</wp:meta_key>
      <wp:meta_value><![CDATA[a:2:{s:4:"time";d:1537461132.405143;s:5:"event";s:9:"check-ham";}]]></wp:meta_value>
    </wp:commentmeta>
  </wp:comment>
</item>


---
title: "¿Aprendiendo a contar? - NSC #4"
categories: ["matemáticas"]
tags: ["cardinales", "fundamentos", "ordinales", "teoría de conjuntos"]
slug: aprendiendo-a-contar-nsc-4
aliases: [/archivo/2018/03/19/aprendiendo-a-contar-nsc-4/]
date: 2018-03-19T15:35:20+0000
lastmod: 2018-03-19T15:35:20+0000
---

En la última entrada mostramos que,
si tenemos una cantidad infinita de elementos
ordenada de una cierta manera,
podemos reordenarla de forma que el resultado sea,
digamos, esencialmente distinto.
Recuerda la carrera con infinitos competidores:
es posible que en la clasificación final nadie quede último (\\(ω\\)),
o que haya un último (\\(ω+1\\)),
o que haya último y penúltimo (\\(ω+2\\)),
o incluso que el resultado sea como dos carreras infinitas sin último,
una detrás de otra (\\(ω\cdot 2\\)).
La manera de demostrar que realmente podíamos hacer esta reorganización
consistía en hacer una tabla con dorsales y posiciones,
de manera que quedara claro que ambas clasificaciones,
la original (según el número del dorsal) y la nueva,
podían ser perfectamente válidas para nuestra carrera.

En particular, el dar ese tipo de tabla
ayuda a demostrar tres hechos acerca de esta reordenación:
<ol>
<li>que todos los competidores que estaban en la clasificación original
aparecían en la clasificación nueva,</li>
<li>que nadie salía dos veces en la clasificación nueva, </li>
<li>que no había dos personas ocupando una misma posición, y</li>
<li>que en la clasificación nueva todos las posiciones estaban ocupadas
por gente de la clasificación vieja.</li>
</ol>
Si lo piensas, esto es lo necesario y suficiente
para poder decir que ambas clasificaciones son válidas.
En jerga, un reordenamiento que cumple (1) y (2)
es un ejemplo de <b>función</b> o <b>aplicación</b>:
una forma de asignar a todo elemento de un conjunto
uno y sólo uno de otro conjunto.
Cuando esta función reordenamiento cumple además (3),
se le llama <b>inyectiva</b>:
dos números/objetos/entes distintos entre sí
deben ser asignados a dos objetos distintos entre sí.
Si la función reordenamiento cumple (4) (además de (1) y (2)),
se dice que es <b>suprayectiva</b>:
todo número/objeto/ente/valor que se puede alcanzar, es alcanzado:
hay otro número/objeto/ente/valor al que le asignas el primero.
Finalmente, si todas las condiciones se cumplen,
tenemos una función reordenamiento <b>biyectiva</b>,
también llamada <b>biyección</b>.

Cuando tienes una biyección entre dos conjuntos \\(A\\) y \\(B\\),
lo que dices es que a todo elemento del primer conjunto \\(A\\)
le asignas uno y sólo uno del segundo conjunto \\(B\\),
y que todo elemento de \\(B\\)
ha sido asignado a uno y sólo uno de \\(A\\).
En cierto modo, tener una biyección entre dos conjuntos
equivale a hacer parejitas entre los elementos de los dos conjuntos,
porque tener un elemento de \\(A\\) (o de \\(B\\))
determina un único elemento de \\(B\\) (o de \\(A\\)).
Fíjate que, aunque en principio el concepto de función
establece una asimetría entre elementos <em>asignados a otros</em>
y elementos <em>a los que asignas otros</em>,
esa asimetría desaparece cuando tienes una biyección.

Si esto suena muy abstracto,
un ejemplo alejado de nuestras carreras infinitas.
En un colegio, los niños se distribuyen por clases,
y cada clase tiene un tutor.
Tú puedes, entonces, definir una función
entre el conjunto de niños y el de profesores:
a cada niño, le asocias el tutor de su clase.
Esto es una función, porque cada niño tiene un y sólo un tutor;
pero no es una biyección, porque en general un tutor lo será de más de un niño,
reflejando que tu función no es inyectiva.
De hecho, si coges un profesor al azar en el colegio,
es posible que no sea tutor de nadie,
lo que indica que tu función tampoco es suprayectiva.
Tendrías una biyección si cada alumno tuviese un tutor único
y no compartido con nadie más;
buena suerte buscando estas biyecciones en los colegios.

Observa, por cierto, lo que pasaría en este último caso biyectivo.
¿Qué me contestarías si te pregunto cuántos alumnos hay en ese colegio?
Probablemente desconocerías la respuesta, sin conocer dicho colegio.
Pero, ¿y si te pido una relación con respecto al número de profesores?:
si supieras cuántos profesores hay, ¿sabrías cuántos alumnos hay?
Si has estado atento, tal vez hayas caído en la cuenta de que,
necesariamente, el número de profesores
ha de ser el mismo que el de alumnos en este mágico colegio.
Esto es una consecuencia de la biyectividad, del hacer parejitas.

Porque esto es contar.
Ya que hoy hablamos de colegios,
¿cómo cuentan los profesores cuántos alumnos hay en su clase?
No miran y adivinan el número por arte de magia (salvo que haya dos alumnos),
sino que van señalando a cada uno,
mentalmente o con el dedo, y asignándoles un número.
Si el último número que el profesor dice es 27,
éste concluye, tal vez inconscientemente,
que existe una biyección entre el conjunto de alumnos de su clase
y el conjunto de números naturales entre 1 y 27.
Y como hay 27 números entre 1 y 27,
debe haber 27 alumnos en la clase.
El que cuenta cuántas palabras hay
en la redacción para su examen de idiomas hace lo mismo,
o el trabajador que cuenta las semanas hasta las vacaciones.
Si no lo habías visto así antes, medita sobre ello.

Pero bueno. No nos hemos pasado las entradas anteriores hablando de infinitos
para ahora no mentarlos, ¿verdad?
Podemos hablar de número de personas/objetos/cosas
cuando tenemos cantidades finitas,
pero para infinitos esto es problemático.
En la entrada anterior introdujimos el concepto de cardinal,
que medía de alguna forma las cantidades incluso para casos infinitos.
Hagámoslo más riguroso.
Para empezar, podemos definir los cardinales de una forma, digamos, relativa:
en vez de hablar de cardinal a secas,
podemos decir si dos objetos <b>tienen el mismo cardinal</b>.
Y para esto, lo único que exigiremos
es que podamos definir una biyección entre ellos.
Ya hemos visto que contar cantidades finitas
está ligado a establecer biyecciones con conjuntos de números,
concretamente los que conocemos como ordinales.
¿Por qué no hacer lo mismo con cantidades infinitas,
y buscar una biyección con un ordinal?
Éste no será único:
ahí tenemos \\(ω\\), \\(ω+1\\), \\(ω\cdot2\\) y demás, biyectivos entre sí.
Pero sí podemos definir el <b>cardinal</b> de un conjunto
como el ordinal más pequeño con el que se puede establecer una biyección.
Así, el cardinal de 42 es 42, porque es finito,
y el de \\(ω\\) es \\(ω\\),
porque es el primer ordinal infinito;
pero el cardinal de \\(ω+1\\) también será \\(ω\\),
y el de \\(ω\cdot 2\\), y así.

Recapitulemos.
Los ordinales, viéndolos como generalización de los números naturales,
no son mal invento para describir
maneras de ordenar cantidades finitas e infinitas de cosas.
Sin embargo, los números naturales
no sólo sirven para indicar orden y posiciones,
sino también para indicar cantidades.
Esto no ocurre con los ordinales infinitos,
para los cuales podemos tener varios ordinales
correspondientes a una misma cantidad,
como se puede ver estableciendo biyecciones entre ellos:
la manera rigurosa (pero bastante real) de contar cosas.
Así que definimos los cardinales como los ordinales
que sí sirven para indicar cantidad:
los finitos (o sea, los números)
y los más pequeños correspondientes a cada cantidad infinita (como \\(ω\\)).

Se me ocurre que en la entrada de hoy
han aparecido bastantes conceptos nuevos e interesantes,
al menos si nunca te habías encontrado con esto,
pero quiero añadir una cosa más.
Tal vez estés pensando que, en el momento que tienes un conjunto infinito,
su cardinal va a ser \\(ω\\).
Es cierto que ambos son conjuntos infinitos,
pero eso no quiere decir que exista una biyección entre ellos.
Y es que una de las cosas más sorprendentes
cuando uno estudia por primera vez los cardinales
es que hay más de un cardinal infinito.
Lo que implica que, dado que \\(ω\\) es el ordinal infinito más pequeño,
existe un ordinal mayor que \\(ω\\)
representando una cantidad mayor que la que representa \\(ω\\), que es infinita.
En términos más populares, <em>algunos infinitos son mayores que otros</em>.
Esto da bastante juego
y probablemente sea el tema de la próxima entrada, antes de pasar a otras temáticas.
Palabra clave: Cantor.

---
title: "Caos en el orden - NSC #3"
categories: ["matemáticas"]
tags: ["fundamentos", "teoría de conjuntos", "cardinales", "ordinales"]
slug: caos-en-el-orden-nsc-3
aliases: [/archivo/2018/03/10/caos-en-el-orden-nsc-3/]
date: 2018-03-10T11:46:19+0000
lastmod: 2018-03-10T11:46:19+0000
---

En una carrera participan 10 personas.
Salen, corren los kilómetros que deban correr
(salvo que sean como yo, en cuyo caso tal vez sólo corran "kilómetro" en singular),
y terminan.
Pongamos que has visto cómo iban llegando,
y que has anotado su orden de llegada.
Una de esas personas habrá llegado en primera posición,
otra en la segunda, y así hasta la décima.
Todo según lo previsto, pues corrían 10 personas:
si hubieras visto que nadie llegaba en décima posición,
tal vez hubieras dado la voz de alarma por una persona
que nunca llegó a la meta;
si hubieras visto que alguien llegaba en la undécima posición,
habrías enarcado una ceja y sospechado juego sucio.
Pero todo sale bien: eran diez personas,
y todas las posiciones de la primera a la décima están cubiertas.

Esto, que es tan evidente que hasta es difícil de explicar,
muestra que existe una relación entre cantidades de cosas y,
digamos, la forma que tendrá la lista de posiciones una vez las hayamos ordenado:
corren diez personas,
luego cubrirás todas las posiciones de la primera a la décima;
haces un ránking con quinientas universidades,
y quedarán cubiertas todas las posiciones de la primera a la quingentésima.
En unos momentos quedará claro
por qué no quiero usar este lenguaje,
pero la manera de verlo es:
el número de cosas que ordenas es el mismo
que el de las posiciones con las que acabas después de ordenar.
Si tienes cien cosas y las ordenas, tendrás cien posiciones distintas.

Bien, no hemos discutido acerca de cómo ordenar infinitas cosas
para ahora no intentar imaginarnos lo que puede ocurrir
cuando tratamos cantidades infinitas.
Imagínate que, en vez de 10 personas,
corre un número infinito de ellas.
Son infinitas, pero cada una tiene un número asociado diferente,
un dorsal que las identifica.
Puedes buscar a la persona con el dorsal 196883 y la encontrarás,
y toda persona tendrá un número.
Es un truco bastante conveniente para comprobar que hay infinitas personas, ¿no?:
dale un número a cada una,
y si queda algún número sin repartir es que no son infinitas.
Pongamos que, para simplificar las cosas,
resulta que llegan justo en el orden que dictaba su dorsal:
la persona con el número 1 llega la primera;
la del número 2, la segunda; y así.
Si los pusieras en orden de llegada, los dorsales tendrían esta pinta:

1, 2, 3, ... ... ... 255, 256, ... ...

Si recuerdas nuestra última discusión,
tal vez veas la relación entre los números naturales y estos corredores:
hay tantos corredores como números naturales,
y el orden en el que llegan es el orden en el que ordenamos los números naturales.
En la jerga que introdujimos,
el ordinal asociado a este conjunto ordenado de personas debería ser \\(\omega\\),
puesto que nadie llega en la posición \\(\omega\\),
pero todas las posiciones por debajo están ocupadas.
Tenemos un ordinal infinito para una cantidad de personas infinita,
así que por ahora todo bien.

O casi. Ricemos el rizo.
Imagínate ahora que, en vez de llegar en el orden que marcaban los dorsales,
resulta que la persona con dorsal número 1 llega la última.
Con "la última" quiero decir que, en la clasificación final,
todos los demás corredores han quedado por delante.
El orden de los demás es el mismo, al menos de forma relativa:
si el dorsal 100 queda por delante de dorsal 101,
ahora también, sólo que dorsal 100 ocupa ahora la posición 99
y dorsal 101, la posición 100.
El esquema es una cosa tal que así:

<table style="border:0;">
<tr>
<td style="border:0;width:100px;">Posición</td>
<td style="border:0;">1</td>
<td style="border:0;">2</td>
<td style="border:0;">3</td>
<td style="border:0;">...</td>
<td style="border:0;">99</td>
<td style="border:0;">100</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;"><em>ω</em></td>
</tr>
<tr>
<td style="border:0;">Dorsal</td>
<td style="border:0;">2</td>
<td style="border:0;">3</td>
<td style="border:0;">4</td>
<td style="border:0;">...</td>
<td style="border:0;">100</td>
<td style="border:0;">101</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">1</td>
</tr>
</table>

Pregunta: ¿en qué posición ha llegado el dorsal 1?
O, dicho de otra manera, ¿qué ordinal corresponde ahora a la clasificación final?
Su posición no puede denotarse con un número.
Si hubiera llegado en la posición 1728,
por poner un ejemplo,
entonces sólo habría quedado por delante un número finito de personas;
como corren infinitas,
esto implica que alguien tiene que quedar necesariamente por detrás,
lo que contradice que "dorsal número 1" haya llegado en último lugar.
Luego se da el caso de que, tal vez,
esta persona haya llegado en la posición \\(\omega\\).
Desde luego, está por detrás de todas las demás,
igual que \\(\omega\\) detrás de todos los números naturales, así que es plausible.
Para estar seguros, habría que comprobar que no hay nadie
que haya quedado <em>en penúltimo lugar</em>,
ya que no hay ningún ordinal que esté
<em>inmediatamente por delante</em> de \\(\omega\\).
Supongamos que alguien hubiera llegado penúltimo.
¿Cuál es su dorsal?
Da igual el número: si lleva el número \\(N\\),
entonces el dorsal con el número \\(N+1\\) debería haber quedado por detrás;
pero por detrás sólo está el dorsal 1, que es el último.
Contradicción; concluimos que nadie ha podido llegar penúltimo.
Así, el dorsal número 1 ocupa la posición \\(\omega\\),
y la clasificación final llevaría asociado
el menor ordinal por encima de \\(\omega\\): esto es, \\(\omega+1\\).

Esto es importante, así que vamos a repetirlo.
Para cantidades finitas de cosas,
ordenarlas da lugar a una lista cuyo ordinal asociado coincide
con la cantidad de las cosas, sin importar cómo las ordenemos.
Sin embargo, tenemos una cantidad infinita de cosas
que podemos reordenar y acabar con distintos ordinales,
sin cambiar de cantidad en el proceso.
O, dicho en la jerga matemática de la última vez:
el ordinal \\(\omega\\) contiene tantos elementos como \\(\omega+1\\),
a pesar de que, a todas luces, \\(\omega+1\\) contenía un elemento más que \\(\omega\\).
Hemos destruido esa conexión entre cantidad y orden
que teníamos con las cosas finitas,
pero al mismo tiempo hemos recuperado
esa intuición extraña que teníamos con el infinito, y que sonaba rara la última vez.
Y es que, en cantidades,
<em>infinito más uno</em> es <em>infinito</em>, no se altera;
pero desde el punto de vista del orden,
<em>infinito más uno</em> sí es distinto que sólo <em>infinito</em>.
Por esto los matemáticos tratan de separar estas dos nociones.
Cuando tienes un conjunto ordenado de esta forma,
tienes un ordinal y un <b>cardinal</b> asociados:
el ordinal indica la manera en la que están ordenados,
y el cardinal indica cuántos elementos tiene tu conjunto.
Si el cardinal es finito,
todas las formas de ordenar tu conjunto llevarán el mismo ordinal:
si corren diez personas,
todas las posiciones entre la primera y la décima estarán ocupadas siempre,
y ninguna más.
Si el cardinal es infinito, en cambio,
es posible reordenar el conjunto de forma que
el ordinal cambie: haz que alguien llegue último.

Vale. Hemos encontrado dos ordinales, \\(\omega\\) y \\(\omega+1\\),
que corresponden a maneras distintas de ordenar un conjunto infinito.
Pregunta: ¿hay alguna otra manera de ordenar un conjunto infinito
que no se corresponda con estos dos ordinales?
Volvamos a nuestra carrera con infinitos participantes.
Esta vez, dorsal número 1 no llega último, sino <em>penúltimo</em>;
será dorsal número 2 quien llegue último.
La clasificación queda

<table style="border:0;">
<tr>
<td style="border:0;width:100px;">Posición</td>
<td style="border:0;">1</td>
<td style="border:0;">2</td>
<td style="border:0;">3</td>
<td style="border:0;">...</td>
<td style="border:0;">98</td>
<td style="border:0;">99</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;"><em>ω</em></td>
<td style="border:0;"><em>ω</em>+1</td>
</tr>
<tr>
<td style="border:0;">Dorsal</td>
<td style="border:0;">3</td>
<td style="border:0;">4</td>
<td style="border:0;">5</td>
<td style="border:0;">...</td>
<td style="border:0;">100</td>
<td style="border:0;">101</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">1</td>
<td style="border:0;">2</td>
</tr>
</table>

¿Cuál es el ordinal ahora?
No puede ser \\(\omega\\) porque hay un último,
ni tampoco \\(\omega+1\\) porque hay un penúltimo.
Puede ser \\(\omega+2\\); de hecho, es \\(\omega+2\\),
y te propongo que trates de demostrar que esto debe ser así.

De forma similar,
podemos concluir que todos los ordinales de la forma \\(\omega+n\\)
se pueden corresponder con nuestra clasificación infinita.
Esto es bastante grave de por sí,
pero ¿hay algún ordinal más al que le pase esto?
Recordarás que, por detrás de los \\(\omega+n\\), venía \\(\omega\cdot 2\\):
¿le ocurre también?
Bueno, imagínate que la carrera acaba así:

<table style="border:0;">
<tr>
<td style="border:0;width:80px;">Posición</td>
<td style="border:0;">1</td>
<td style="border:0;">2</td>
<td style="border:0;">3</td>
<td style="border:0;">4</td>
<td style="border:0;">5</td>
<td style="border:0;">...</td>
<td style="border:0;"><em>ω</em></td>
<td style="border:0;width:40px;"><em>ω</em>+1</td>
<td style="border:0;width:40px;"><em>ω</em>+2</td>
<td style="border:0;width:40px;"><em>ω</em>+3</td>
<td style="border:0;width:40px;"><em>ω</em>+4</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">Dorsal</td>
<td style="border:0;">2</td>
<td style="border:0;">4</td>
<td style="border:0;">6</td>
<td style="border:0;">8</td>
<td style="border:0;">10</td>
<td style="border:0;">...</td>
<td style="border:0;">1</td>
<td style="border:0;">3</td>
<td style="border:0;">5</td>
<td style="border:0;">7</td>
<td style="border:0;">9</td>
<td style="border:0;">...</td>
</tr>
</table>

Vamos, que los pares acaban por detrás que los impares.
Hay infinitos números por delante de dorsal 1,
así que tiene sentido que su posición sea \\(\omega\\);
sin embargo, también hay infinitos números <em>por detrás</em>,
así que necesitamos un ordinal que refleje este hecho.
En este caso, sí, \\(\omega\cdot2\\) es el ordinal adecuado: puedes comprobarlo.
Y algo parecido, repartiéndolo los números en tres bloques,
para \\(\omega\cdot3\\); o en cuatro bloques, para \\(\omega\cdot4\\)...

Y, si ocurre para todos los \\(\omega\cdot n\\),
¿ocurre también para \\(\omega^2\\)?
Poner infinitos bloques infinitos de números
(has contado bien: hay dos infinitos ahí),
uno detrás de otro, es difícil y no refleja bien el patrón a seguir.
Así que los voy a poner uno debajo de otro. El resultado queda

<table style="border:0;">
<tr>
<td style="border:0;width:80px;">Posición</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;">0</td>
<td style="border:0;">1</td>
<td style="border:0;">2</td>
<td style="border:0;">3</td>
<td style="border:0;">4</td>
<td style="border:0;">5</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;"><em>ω</em></td>
<td style="border:0;width:40px;"><em>ω</em>+1</td>
<td style="border:0;width:40px;"><em>ω</em>+2</td>
<td style="border:0;width:40px;"><em>ω</em>+3</td>
<td style="border:0;width:40px;"><em>ω</em>+4</td>
<td style="border:0;width:40px;"><em>ω</em>+5</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;"><em>ω</em>·2</td>
<td style="border:0;width:40px;"><em>ω</em>·2+1</td>
<td style="border:0;width:40px;"><em>ω</em>·2+2</td>
<td style="border:0;width:40px;"><em>ω</em>·2+3</td>
<td style="border:0;width:40px;"><em>ω</em>·2+4</td>
<td style="border:0;width:40px;"><em>ω</em>·2+5</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;"><em>ω</em>·3</td>
<td style="border:0;width:40px;"><em>ω</em>·3+1</td>
<td style="border:0;width:40px;"><em>ω</em>·3+2</td>
<td style="border:0;width:40px;"><em>ω</em>·3+3</td>
<td style="border:0;width:40px;"><em>ω</em>·3+4</td>
<td style="border:0;width:40px;"><em>ω</em>·3+5</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;"><em>ω</em>·4</td>
<td style="border:0;width:40px;"><em>ω</em>·4+1</td>
<td style="border:0;width:40px;"><em>ω</em>·4+2</td>
<td style="border:0;width:40px;"><em>ω</em>·4+3</td>
<td style="border:0;width:40px;"><em>ω</em>·4+4</td>
<td style="border:0;width:40px;"><em>ω</em>·4+5</td>
<td style="border:0;">...</td>
<td style="border:0;">
</tr>
<tr>
<td style="border:0;"></td>
<td style="border:0;">...</td>
<tr>

<td style="border:0;">Dorsal</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;">0</td>
<td style="border:0;">1</td>
<td style="border:0;">3</td>
<td style="border:0;">6</td>
<td style="border:0;">10</td>
<td style="border:0;">15</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;">2</td>
<td style="border:0;">4</td>
<td style="border:0;">7</td>
<td style="border:0;">11</td>
<td style="border:0;">16</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;">5</td>
<td style="border:0;">8</td>
<td style="border:0;">12</td>
<td style="border:0;">17</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;">9</td>
<td style="border:0;">13</td>
<td style="border:0;">18</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;">
</td>
<td style="border:0;">14</td>
<td style="border:0;">19</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
<td style="border:0;">...</td>
</tr>
<tr>
<td style="border:0;"></td>
<td style="border:0;">...</td>
</tr>
</table>

¿Ves el patrón? Porque si lo ves tengo un reto para ti:
¿podrías generalizar esto para \\(\omega^\omega\\)?
Tengo alguna idea acerca de cómo hacerlo;
la pondría aquí pero, parafraseando a Fermat,
el espacio que me queda en esta entrada
es demasiado pequeño para escribir mi solución.

Una última pregunta para que medites.
En la jerga que hemos introducido,
todos los ordinales entre \\(\omega\\) y \\(\omega^\omega\\)
tienen el mismo cardinal.
Cabe suponer que, si seguimos subiendo,
sumando, multiplicando o elevando omegas,
el resultado tendrá el mismo cardinal.
Pero entonces podrías preguntarte:
¿hay algún ordinal que tenga un cardinal distinto a éste?
En otras palabras, ¿hay una cantidad infinita que sea <em>mayor</em>
que esta cantidad infinita?

Antes de terminar, una aclaración.
En la entrada sobre ordinales, y también cuando describimos los números,
todas nuestras listas empezaban con el número cero.
Para esta entrada decidí omitir el cero,
por aquello de que íbamos a tratar ejemplos "reales"
donde, al clasificar, nunca empezamos por el cero, sino por el uno.
Son únicamente motivos prácticos,
no es que de pronto el cero ya no sea útil.
De hecho, puedes reescribir todo lo que hemos hecho,
pero esta vez usando el cero:
imagínate que el dorsal 1 es el realidad el 0, el 2 es el 1, y así.
El resultado es el mismo.
Ahí tienes el ejemplo de \\(\omega^2\\),
donde por simetría era más conveniente usar el cero: nada cambia.
Y tal vez esto explique mejor una cierta asimetría
que hay al principio de esta entrada.
El ordinal de una lista infinita
en la que hay un último y no un penúltimo es \\(\omega+1\\)
porque hay un elemento en la posición \\(\omega\\)
pero ninguno en la \\(\omega+1\\).
Sin embargo, el ordinal de la carrera con diez personas era el 10
porque la última posición era 10 y no 11 o 9.
Si metes el cero en la carrera de diez personas,
verás que ahora la lista acaba en 9;
asignarle el ordinal 10 encaja mejor con lo que hacíamos en el caso infinito.

<item>
  <title>Agrupando simetrías - NSC #12</title>
  <link>https://nosolocuentas.wordpress.com/2018/09/20/agrupando-simetrias-nsc-12/</link>
  <pubDate>Thu, 20 Sep 2018 15:20:33 +0000</pubDate>
  <dc:creator>guicafer</dc:creator>
  <guid isPermaLink="false">https://nosolocuentas.wordpress.com/?p=71</guid>
  <description/>
  <content:encoded><![CDATA[<a href="https://nosolocuentas.wordpress.com/2018/09/06/agrupando-permutaciones-nsc-11/">La última vez</a> vimos que los reordenamientos de <em>n</em> bolas, lo que podríamos considerar sus simetrías, forman un grupo, el <em>grupo de permutaciones de <em>n</em> elementos</em>, denotado <em>S<sub>n</sub></em>. Tal vez te preguntes qué tiene que ver esto con simetrías, digamos, usuales: por ejemplo, las de figuras geométricas sencillas. ¿Qué dice esto acerca de las simetrías de un triángulo equilátero, por ejemplo? Bien, si damos nombre a los vértices del triángulo, recuperamos una configuración de tres bolas, casi como la de la última vez:

<img class=" size-full wp-image-80 aligncenter" src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-0.png" alt="Triángulo equilátero, sus vértices son bolas etiquetadas A, B, y C." width="504" height="450" />

Digo "casi" porque ahora están conectadas por los lados del triángulo, mientras que antes nuestros dibujos asumían que las bolas estaban en fila, para no hacer gamberradas con las bolas y llamarlas reordenamientos. Una transformación de simetría del triángulo debería mantener la forma general del triángulo, permitiéndose a lo sumo permutar los vértices, por lo que todas las transformaciones de simetría del triángulo van a venir dadas por elementos de <em>S<sub>3</sub></em>. En principio, el no poder deformar los lados del triángulo puede significar que no todas las permutaciones dan lugar a simetrías, así que habría que ir con cuidado. Pero resulta lo de los lados del triángulo no influye demasiado en este caso. Realmente, si piensas en una permutación de <em>S<sub>3</sub></em>, vas a obtener una transformación de simetría del triángulo siempre. ¿Te gusta el ciclo <em>(1 2 3)</em>? Bien, esto no es más que una <em>rotación</em> de 120°, o <em>2π/3</em> si lo prefieres, en el sentido contrario al de las agujas de cualquier reloj con agujas:

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-1.png" alt="El triángulo de antes, rotado 120 grados." width="1213" height="532" class="aligncenter size-full wp-image-79" />

¿Preferías algo como un <em>(2 3)</em>? Esto es una <em>reflexión</em> respecto del eje vertical:

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-2.png" alt="El mismo triángulo, reflejado respecto de su eje vertical." width="1213" height="710" class="aligncenter size-full wp-image-78" />

Hay otras cuatro permutaciones (recuerda que <em>S<sub>3</sub></em> tiene <em>3! = 6</em> elementos); te dejo a ti comprobar que realmente dan lugar a simetrías del triángulo.

Subamos el nivel: ¿qué simetrías tiene el cuadrado?

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-3.png" alt="Cuadrado, sus vértices son bolas con etiquetas A, B, C, y D." width="567" height="568" class="aligncenter size-full wp-image-77" />

Igual que antes, todas las simetrías del cuadrado deberían ser expresables como permutaciones de <em>S<sub>4</sub></em>. Ahora, el grupo <em>S<sub>4</sub></em> tiene <em>4! = 24</em> elementos, pero resulta que no todos van a dar lugar a simetrías. Algunos sí, como la permutación <em>(1 2 3 4)</em>,

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-4.png" alt="El mismo cuadrado, rotado 90 grados." width="1276" height="568" class="aligncenter size-full wp-image-76" />

dando lugar a una rotación de 90°, o <em>2π/4</em>, en el sentido contrario al de las agujas del reloj. Pero usa ahora la permutación <em>(1 2)</em>,

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-5.png" alt="El cuadrado, deformado y retorcido tras intercambiar dos vértices contiguos." width="1276" height="568" class="aligncenter size-full wp-image-75" title="Con esto, los matemáticos descubrieron un eficaz método de tortura a polígonos." />

e incurres en violencia geométrica. (Vamos a ver, serán figuras abstractas, pero aquí me encariño y empatizo con las abstracciones y siento que me retuerces el pescuezo con cosas así.) Esto no puede ser una transformación de simetría: no puedes intercambiar dos bolas contiguas sin más. Igualmente, tampoco valen las permutaciones <em>(2 3)</em>, <em>(3 4)</em> o <em>(1 4)</em>. Pero esto tampoco quiere decir que no valgan todas las permutaciones hechas de ciclos de longitud 2: permutar según <em>(2 4)</em>

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-6.png" alt="El cuadrado, tras una reflexión según el eje vertical (una diagonal del cuadrado)." width="1276" height="710" class="aligncenter size-full wp-image-74" />

da lugar a una reflexión según la vertical que pasa por <em>A</em> y <em>C</em>. La reflexión respecto de la horizontal vendría dada por <em>(1 3)</em>. Y, aunque no valgan ni <em>(1 2)</em> ni <em>(3 4)</em>, <em>su producto sí</em>:

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-7.png" alt="El cuadrado, reflejado respecto de un eje perpendicular a dos lados." width="1276" height="568" class="aligncenter size-full wp-image-73" />

esto representaría la reflexión respecto de un eje diagonal, pasando por los centros de dos lados paralelos. E, igualmente, <em>(2 3)(1 4)</em> representa la reflexión respecto del eje horizontal. También hay otra combinación de 2-ciclos, la <em>(1 3)(2 4)</em>: individualmente daban lugar a transformaciones de simetría, luego su producto debería ser otra también; y efectivamente,

<img src="https://nosolocuentas.files.wordpress.com/2018/09/nsc12fig-8.png" alt="El cuadrado, tras intercambiar vértices no contiguos." width="1276" height="568" class="aligncenter size-full wp-image-72" />

obtenemos el resultado de intercambiar los vértices según los ejes vertical y horizontal, que es lo mismo que una rotación de 180° o <em>4π/4</em>, según o contrario a las agujas del reloj.

El resto de giros y reflexiones podemos construirlos a partir de los que ya tenemos. Las transformaciones de simetría a las que llegamos son
<ul>
	<li>la identidad: <em>1</em>,</li>
	<li>giro de 90° (antihorario): <em>(1 2 3 4)</em>,</li>
	<li>giro de 180°: <em>(1 3)(2 4) = (1 2 3 4)(1 2 3 4) = (1 2 3 4)<sup>2</sup></em>,</li>
	<li>giro de 270°: <em>(1 4 3 2) = (1 2 3 4)(1 3)(2 4) = (1 2 3 4)<sup>3</sup></em>,</li>
	<li>reflexión respecto de la diagonal <em>B—D</em>: <em>(1 3)</em>,</li>
	<li>reflexión respecto de la diagonal <em>A—C</em>: <em>(2 4) = (1 3)(1 3)(2 4) = (1 3)(1 2 3 4)<sup>2</sup></em>,</li>
	<li>reflexión respecto de la vertical: <em>(1 2)(3 4) = (1 3)(1 2 3 4)</em>,</li>
	<li>reflexión respecto de la horizontal: <em>(1 4)(2 3) = (1 3)(1 4 3 2) = (1 3)(1 2 3 4)<sup>3</sup></em>.</li>
</ul>

Vayamos por partes. Sabemos que las transformaciones de simetría tienen que formar un grupo: básicamente, por sentido común, y porque si no lo que estamos haciendo pasa a ser un nuevo error en nuestra vida. Desde luego, la composición de operaciones de simetría se traduce en el producto de permutaciones, que es el producto de <em>S<sub>4</sub></em>. El producto de dos de las transformaciones que tenemos vuelve a ser una de las transformaciones que tenemos. La identidad es una simetría de las que tenemos, y los inversos también: las reflexiones son sus propios inversos, y el inverso de una rotación es otra rotación. ¿Es el producto asociativo? Bueno, sí, porque es el de <em>S<sub>4</sub></em>, y ahí era asociativo. Luego tenemos un grupo formado por permutaciones de <em>S<sub>4</sub></em>, usando la operación de <em>S<sub>4</sub></em>, pero que no engloba a todas las permutaciones de <em>S<sub>4</sub></em>: lo que decimos es que tenemos un <b>subgrupo de </b><em>S<sub>4</sub></em>. El nuevo grupo tiene nombre, por supuesto: se denomina <b>grupo diédrico de orden 8</b>, y se representa como <em>D<sub>4</sub></em>. Es algo confuso que el subíndice sea un 4 pero se diga <em>de orden 8</em>: el subíndice hace referencia al hecho de que actúa sobre un cuadrado, con cuatro vértices, mientras que el orden indica que hay ocho elementos en el grupo.

Otra cosa sobre la que quería llamar tu atención es la siguiente. En la lista de elementos de <em>D<sub>4</sub></em> he incluido la manera de expresar cada uno de ellos en forma de productos involucrando sólo dos permutaciones: la <em>(1 2 3 4)</em> y la <em>(1 3)</em>. Este tipo de información tiene su interés. Imagínate que, en lugar de darte todos los elementos de <em>D<sub>4</sub></em>, lo que te digo es: el grupo de simetrías del cuadrado es el subgrupo de <em>S<sub>4</sub></em> más pequeño que contiene a <em>(1 2 3 4)</em> y <em>(1 3)</em>. Por un lado, sabiendo que es un grupo, puedes deducir que contiene a todos los posibles productos de estas dos permutaciones, con lo que recuperas la lista de las ocho transformaciones de simetría. Como esas transformaciones forman un grupo, ya sabes que forman <em>el</em> grupo de simetrías del cuadrado. Así que estás dando la misma información. Pero, por otro lado, la estás dando de forma más abreviada, lo que ahorra espacio y ayuda a alcanzar la iluminación. Porque, piénsalo: ¿no te parece intelectualmente menos satisfactorio pensar en las simetrías del cuadrado como una lista, que pensar en ellas como todas las maneras posibles de combinar rotaciones de 90° y una reflexión? De la segunda forma, has simplificado el estudio de las simetrías del cuadrado hasta alcanzar lo verdaderamente importante: todas están basadas en dos simetrías, si conoces ésas las conoces todas. Los matemáticos tienen nombre para este fenómeno: dicen que <em>D<sub>4</sub></em> <b>está generado por </b><em>(1 2 3 4)</em><b> y </b><em>(1 3)</em>.

No sólo eso, sino que también te ayuda a entender el resto de polígonos regulares. El grupo de simetrías del triángulo equilátero también está generado por una rotación y una reflexión, lo que ocurre es que casualmente este grupo engloba a todas las permutaciones de <em>S<sub>3</sub></em>. El del pentágono regular también está generado por una rotación y una reflexión; y el del hexágono, y el del heptágono... En general, para un polígono regular de <em>n</em> lados, obtienes que su grupo de simetrías es <em>D<sub>n</sub></em>, el grupo diédrico de orden <em>2n</em>, generado por una rotación y una reflexión. Estos grupos son distintos de <em>S<sub>n</sub></em>, salvo para el caso del triángulo equilátero, dado que su grupo es <em>S<sub>3</sub></em> y, por lo tanto, <em>D<sub>3</sub>=S<sub>3</sub></em>.

Por supuesto, uno puede preguntarse qué ocurre con otras figuras geométricas, como los polígonos no regulares o poliedros y demás. Si tu polígono de <em>n</em> lados no es un polígono regular, vas a tener menos simetrías que si lo fuera, así que su grupo de simetrías va a ser un subgrupo de <em>D<sub>n</sub></em>; el tamaño de dicho subgrupo tendrá que ver con el número de simetrías que le queden a tu polígono. Mientras que si lo que tienes es un poliedro o algo en más dimensiones, sus simetrías no van a venir dadas por un grupo diédrico. Pero tienen sus grupos igualmente, que serán subgrupos de un <em>S<sub>n</sub></em> según el número <em>n</em> de vértices que tenga. Un ejemplo sencillo, y con esto ya acabo: el <em>n</em>-<b>símplex</b>, que es la generalización del triángulo equilátero a <em>n</em> dimensiones, tiene como grupo de simetrías <em>S<sub>n+1</sub></em>; el "<em>n+1</em>" viene de que tiene <em>n+1</em> vértices. Ya hemos visto que el triángulo equilátero, que vive en 2 dimensiones, tiene como grupo el <em>S<sub>3</sub></em>; en el caso del tetraedro, se puede ver que toda permutación de <em>S<sub>4</sub></em> es una simetría del tetraedro, así que su grupo es <em>S<sub>4</sub></em>. La idea es que en un <em>n</em>-símplex todos los vértices están conectados entre sí, así que no hay ninguna permutación que induzca un retorcimiento de la figura, y por eso su grupo de simetría sería <em>S<sub>n+1</sub></em>. Pero en fin, dije que acababa y acabo.]]></content:encoded>
  <excerpt:encoded><![CDATA[]]></excerpt:encoded>
  <wp:post_id>71</wp:post_id>
  <wp:post_date>2018-09-20 15:20:33</wp:post_date>
  <wp:post_date_gmt>2018-09-20 15:20:33</wp:post_date_gmt>
  <wp:post_modified>2018-09-20 15:20:34</wp:post_modified>
  <wp:post_modified_gmt>2018-09-20 15:20:34</wp:post_modified_gmt>
  <wp:comment_status>open</wp:comment_status>
  <wp:ping_status>open</wp:ping_status>
  <wp:post_name>agrupando-simetrias-nsc-12</wp:post_name>
  <wp:status>publish</wp:status>
  <wp:post_parent>0</wp:post_parent>
  <wp:menu_order>0</wp:menu_order>
  <wp:post_type>post</wp:post_type>
  <wp:post_password/>
  <wp:is_sticky>0</wp:is_sticky>
  <category domain="category" nicename="algebra"><![CDATA[Álgebra]]></category>
  <category domain="category" nicename="fundamentos"><![CDATA[Fundamentos]]></category>
  <category domain="post_tag" nicename="grupo-simetrico"><![CDATA[Grupo simétrico]]></category>
  <category domain="post_tag" nicename="grupos"><![CDATA[Grupos]]></category>
  <category domain="post_tag" nicename="simetrias"><![CDATA[Simetrías]]></category>
  <wp:postmeta>
    <wp:meta_key>_publicize_job_id</wp:meta_key>
    <wp:meta_value><![CDATA[22349857086]]></wp:meta_value>
  </wp:postmeta>
  <wp:postmeta>
    <wp:meta_key>timeline_notification</wp:meta_key>
    <wp:meta_value><![CDATA[1537456839]]></wp:meta_value>
  </wp:postmeta>
  <wp:postmeta>
    <wp:meta_key>_rest_api_published</wp:meta_key>
    <wp:meta_value><![CDATA[1]]></wp:meta_value>
  </wp:postmeta>
  <wp:postmeta>
    <wp:meta_key>_rest_api_client_id</wp:meta_key>
    <wp:meta_value><![CDATA[-1]]></wp:meta_value>
  </wp:postmeta>
</item>


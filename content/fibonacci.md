---
title: "Fibonacci y el número áureo"
date: "2023-02-19T20:42:38+0200"
---

Versión 0.0.1 del 19 de febrero de 2023.

# El número áureo en las flores y piñas

Se dice a menudo que se puede ver la sucesión de Fibonacci en las espirales de los girasoles, las piñas y otras plantas.
La explicación partiría de la premisa de que muchas de estas plantas han evolucionado para hacer crecer sus hojas según ángulos concretos.
Esto es: una vez nace una hoja, la segunda nacerá a un ángulo concreto de ésta, y la tercera a ese mismo ángulo de la segunda, y así.
Estos ángulos serían tales que las nuevas hojas crecerían sin obstaculizarse unas a otras: no debajo o encima de las anteriores, sino por entre los huecos que las hojas previas han dejado.
Para que las hojas no se superpongan, es necesario que el ángulo no sea una fracción racional de una vuelta: si esta fracción fuera de la forma a/b con a y b enteros, tras b iteraciones se llegaría a una hoja que nace alineada con una hoja previa.
Luego es necesario quee l ánuglo se construya dividiendo 1 vuelta entre un número irracional.
De todas formas, escoger un número irracional no es garantía de éxito, puesto que hay irracionales que son fácilmente aproximables por racionales,
lo que significa que esperar b iteraciones no nos llevaría a una hoja matemáticamente alineada con una anterior, pero sí a una que, en la práctica, sí está alineada.
El ángulo óptimo vendría dado por un número irracional difícilmente aproximable por números racionales... lo que nos lleva al número áureo.

La segunda parte de esta historia es que esas aproximaciones racionales a la fracción irracional son lo que dan lugar a las espirales.
Si tu ángulo se puede aproximar por una fracción a/b de una vuelta, tras b iteraciones no obtendrás una hoja alineada exactamente con una de las anteriores, pero casi: estará ligeramente desviada hacia un lado, según si la aproximación a/b se queda corta o se pasa del ángulo.
Tras otras b iteraciones, aparecerá una nueva hoja que estará casi alineada con esas dos anteriores, pero que parecerá de nuevo adelantada o retrasada con respecto a las anteriores.
Tras múltiples iteraciones y múltiples vueltas, lo que habría sido una línea recta de hojas alineadas se ha convertido en una curva en espiral.
¿Cuántas de estas espirales hay? Habría unas b: una por cada iteración en la que no aparecen hojas alineadas con las anteriores.

¿Qué ocurre entonces con los girasoles?
Éstos harían crecer sus pepitas en un ángulo de 1/phi, y por tanto no salrían radios rectos exactos de pepitas alineadas, sino espirales de pepitas casi alineadas.
¿Cuántas? Vamos a suponer (porque no sé cómo funciona en la realidad, pero no me importa) que las pepitas crecen en sentido antihorario.
La fracción 1/phi es aproximadamente 0,618.
Sabemos que los cocientes de términos sucesivos de la sucesión de Fibonacci dan lugar a aproximaciones al número áureo, cada vez mejores según avanzamos en la sucesión.
El cociente 8/5, por ejemplo, es igual a 1,6, que es ligeramente inferior a phi;
su inverso, 5/8, es ligeramente superior a 1/phi.
Esto supone que, tras haber crecido 8 pepitas de girasol siguiendo un ángulo aproximadamente igual a un phi-avo de vuelta, la novena pepita va a salir casi alineada con la primera, sólo que un poco retrasada: crecer según ángulos de 1/phi de vuelta es un poco menos que crecer según 5/8 de vuelta.
Crecen otras 8 pepitas, las 8 últimas ligeramente retrasadas con respecto a las 8 primeras, y aparece la decimoséptima pepita:
acumula ese déficit angular con respecto de las pepitas anteriores, por lo que vuelve a quedar retrasada con respecto de la anterior.
Conforme pasan más y más iteraciones, estas pepitas que no consiguen alinearse con sus predecesoras acaban formando una espiral;
dado que las pepitas crecían en sentido antihorario, la espiral causada por estas pepitas parecerá ir en sentido horario.
¿Cuántas habrá de este tipo? Exactamente 8.

Pero en los girasoles se ven espirales en sentidos horario y antihorario, ¿dónde está la otra?
Como decíamos, phi se puede aproximar por muchos cocientes de la sucesión de Fibonacci.
Si en lugar de usar 8/5 usamos 13/8, obtenemos 1,625, que es ligeramente superior al valor de phi.
Por lo que 8/13 es una aproximación por debajo al valor de 1/phi.
Mientras esas pepitas iban creciendo y formando esas 8 espirales en sentido horario,
resultaba que la decimocuarta pepita casi se alinea con la primera, pero esta vez pasándose de largo en lugar de quedándose corto.
Cada 13 iteraciones, las pepitas se añaden a unas espirales que crecen en sentido antihorario, ya que cada pepita está adelantada con respecto a las que vinieron antes.
Aparecen así 13 espirales en sentido antihorario, en el mismo girasol en el que ya hay 8 espirales en sentido horario.

Lo gracioso es que esto no acaba aquí.
Igual que 5/8 aproxima 1/phi por abajo, y 8/13 aproxima 1/phi por arriba,
13/21 vuelve a aproximar 1/phi por abajo, de manera más ajustada que 5/8.
Es posible que, si nuestro girasol tiene las suficientes pepitas, veamos que hay unas espirales en sentido horario que no son las que habíamos contado antes.
Estas espirales serían mucho más rectas que las anteriores, dado que la diferencia entre 1/phi y 13/21 es mucho menor que antes.
Siendo más rectas, caben muchas más: en concreto, habrá 21 espirales de este tipo.

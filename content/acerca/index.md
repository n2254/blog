---
title: "Acerca de"
date: 2018-01-31T12:55:07+0000
lastmod: 2023-02-03T16:30:31+0200
---

Siempre he descrito mis blogs como "en construcción". Y es cierto: abro una página, publico algunas entradas, empiezo a darle forma... y nunca termino. Esta página está aquí para que yo tenga algún sitio donde compartir las cosas que he aprendido (a menudo, en matemáticas), incluso si nunca llego a hacerlo. Espero que pueda serle de utilidad a alguien.

<!--
  <link>https://nosolocuentas.wordpress.com/acerca-de/</link>
  <pubDate>Wed, 31 Jan 2018 12:55:07 +0000</pubDate>
  <dc:creator>guicafer</dc:creator>
  <guid isPermaLink="false">https://nosolocuentas.wordpress.com/?page_id=2</guid>
  <description/>
  <content:encoded><![CDATA[Blog en construcción, más o menos. Se supone que de matemáticas, también más o menos.

En cuanto al autor: Guillermo Fernández Castro (Gijón, 1993) estudió un doble grado de Matemáticas y Física en la Universidad de Oviedo y un máster en Matemáticas por la VU Amsterdam. Ahora es estudiante de doctorado en la TU Delft, y seguirá estudiando si le dejan. Le gusta aprender de todo y hablar de poco. Seguidor de blogs de divulgación desde su adolescencia, se decidió a abrir el blog mientras terminaba el máster, estando por tierras holandesas. Confía en poder aprender a transmitir ideas coherentemente a un público general a medida que el número de entradas publicadas aumenta.]]></content:encoded>
  <excerpt:encoded><![CDATA[]]></excerpt:encoded>
  <wp:post_id>2</wp:post_id>
  <wp:post_date>2018-01-31 12:55:07</wp:post_date>
  <wp:post_date_gmt>2018-01-31 12:55:07</wp:post_date_gmt>
  <wp:post_modified>2020-11-15 17:05:26</wp:post_modified>
  <wp:post_modified_gmt>2020-11-15 17:05:26</wp:post_modified_gmt>

-->
